package com.chrubretonneau.swallowinsound;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class rapportAdapter extends BaseAdapter
{
    // Une liste d'examen détaillés
    private List<List<String>> listeDeglutitions;

    //Le contexte dans lequel est présent notre adapter
    private Context context;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater inflater;


    public rapportAdapter(Context unContext, List<List<String>> uneListeDeglutitions) {
        context = unContext;
        listeDeglutitions = uneListeDeglutitions;
        inflater = LayoutInflater.from(unContext);
    }

    @Override
    public int getCount() {
        return listeDeglutitions.size();
    }

    @Override
    public Object getItem(int i) {
        return listeDeglutitions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem;
        //(1) : Réutilisation des layouts
        if (convertView == null) {
            //Initialisation de notre item à partir du  layout XML "layout_itemdeglutition.xml"
            layoutItem = (LinearLayout) inflater.inflate(R.layout.layout_itemdeglutition, parent, false);
        } else {
            layoutItem = (LinearLayout) convertView;
        }

        //(2) : Récupération des TextView de notre layout
        TextView txtType = (TextView)layoutItem.findViewById(R.id.row_type);
        TextView txtDuree = (TextView)layoutItem.findViewById(R.id.row_duree);

        //(3) : Renseignement des valeurs
        txtType.setText(listeDeglutitions.get(position).get(0));
        txtDuree.setText(listeDeglutitions.get(position).get(1));

        //On retourne l'item créé.
        return layoutItem;
    }
}
