package com.chrubretonneau.swallowinsound;


import android.content.Context;
import android.util.AttributeSet;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;

//Custom view pager
public class CustomViewPager extends ViewPager {
    //Permet de disable le scoll sur toute les veiw pager utilisé dans l'affichage des déglutitions
    private Boolean disable = false;

    //Set le contexte du pager
    public CustomViewPager(Context context) {
        super(context);
    }
    //Même chose avec des attribus
    public CustomViewPager(Context context, AttributeSet attrs){
        super(context,attrs);
    }
    //Ici on vient override la condition pour bloquer intercept le touch event
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !disable && super.onInterceptTouchEvent(event);
    }
    //Ici on désactve le touch event
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !disable && super.onTouchEvent(event);
    }
    //Ici on désactive le scoll sur le pager
    public void disableScroll(Boolean disable){
        //When disable = true not work the scroll and when disble = false work the scroll
        this.disable = disable;
    }
}
