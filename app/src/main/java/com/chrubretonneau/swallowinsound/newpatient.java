package com.chrubretonneau.swallowinsound;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExaminateur;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoPathologie;
import com.chrubretonneau.swallowinsound.ModelesClass.Examinateur;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class newpatient extends Activity {

//region Variables
    //region Variable pour auto-completion examinateurs
        List<Examinateur> examinateurs;
        AutoCompleteTextView txtNomExaminateur;
        List<String> noms = new ArrayList<>();
        int idNomExaminateur;
    //endregion

    //region Variable pour auto-completion pathologie
        List<String> pathologies = new ArrayList<>();
        AutoCompleteTextView txtPathologie;
        int idPathologie;
    //endregion

    //region Variables DatePicker Birthday
        private TextView txtBirthday;
        private DatePicker dpBirthday;

        private int year;
        private int month;
        private int day;

        static final int DATE_DIALOG_ID = 999;
    //endregion
   int idPatient = -1;
//endregion

//region onCreate : Listener pour la date d'anniverssaire + Chargement des Examinateurs pour l'auto-completion avec Listener
    /**
     * Au chargement de l'activité on charge les champs auto-completés de la pathologie et du nom et prénom de l'examinateur. <br/>
     * On met également au place la popup Calendar pour entrer la date de naissance.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpatient);

        setCurrentDateOnView();
        addListenerOnTxtBirtday();

        DaoExaminateur bddExaminateur = new DaoExaminateur(this);
        examinateurs = bddExaminateur.getAllExaminateurs();
        DaoPathologie bddPathologie = new DaoPathologie(this);
        pathologies = bddPathologie.getAllPathologies();
        bddPathologie.close();
        bddExaminateur.close();

        int i = 0;
        for(i=0;i<examinateurs.size();i++)
        {
            noms.add(examinateurs.get(i).getNom());
        }

        txtPathologie = (AutoCompleteTextView) findViewById(R.id.txtPathologie);
        txtNomExaminateur = (AutoCompleteTextView) findViewById(R.id.txtNomExaminateur);

        ArrayAdapter<String> adapterPathologies = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pathologies);
        ArrayAdapter<String> adapterNoms = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, noms);

        txtPathologie.setAdapter(adapterPathologies);
        txtNomExaminateur.setAdapter(adapterNoms);

        txtNomExaminateur.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE)
                {
                    btValiderOnClick(v);

                    return true;
                }
                return false;
            }
        });
    }
//endregion

// region DatePicker Birthday
    public void setCurrentDateOnView() {

        txtBirthday = (TextView) findViewById(R.id.txtBirthday);
        dpBirthday = (DatePicker) findViewById(R.id.dpBirthday);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH)+1;
        day = c.get(Calendar.DAY_OF_MONTH);

        dpBirthday.init(year, month, day, null);

    }

    public void addListenerOnTxtBirtday() {

        txtBirthday = (TextView) findViewById(R.id.txtBirthday);

        txtBirthday.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month,day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth+1;
            day = selectedDay;

            if(month < 10)
            {
                txtBirthday.setText(day + "-0" + month + "-" + year);
            }
            else
            {
                txtBirthday.setText(day + "-" + month + "-" + year);
            }
            // set selected date into datepicker also
            dpBirthday.init(year, month, day, null);

        }
    };
//endregion

//region btValider : Insertion Patient dans base de données
    /**
     * Fonction appelée sur le clique du bouton valider. <br/>
     * Vérifie que tout les champs du patient et de l'examinateur sont remplis, <br/>
     * puis enregistre les différentes informations dans la base de données. <br/>
     * Une fois les traitements fini on bascule vers la page de repas en indiquant dans l'intent l'id du patient.
     * @param view Correspond à la vue courante de l'activité en cours, de type View
     */
    public void btValiderOnClick(View view)
    {
        TextView txtNom = (TextView) findViewById(R.id.txtNomPatient);
        TextView txtPrenom = (TextView) findViewById(R.id.txtPrenomPatient);
        ImageView leftValider = (ImageView) findViewById(R.id.leftValider);
        Button btValider = (Button) findViewById(R.id.btValider);
        ImageView rigthValider = (ImageView) findViewById(R.id.rightValider);

        leftValider.setBackground(getResources().getDrawable(R.drawable.pressedleft));
        btValider.setBackground(getResources().getDrawable(R.drawable.pressedmiddle));
        rigthValider.setBackground(getResources().getDrawable(R.drawable.pressedrigth));

        leftValider.setBackground(getResources().getDrawable(R.drawable.unpressedleft));
        btValider.setBackground(getResources().getDrawable(R.drawable.unpressedmiddle));
        rigthValider.setBackground(getResources().getDrawable(R.drawable.unpressedrigth));

        boolean ok = false;

        if(txtNom.getText().toString().equals("") || txtPrenom.getText().toString().equals("") || txtBirthday.getText().toString().equals("") || txtPathologie.getText().toString().equals(""))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Alerte")
                    .setMessage("Veuillez renseigner toutes les données du patient")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Some stuff to do when ok got clicked
                        }
                    })
                    .show();
        }
        else if(txtNomExaminateur.getText().toString().equals(""))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Alerte")
                    .setMessage("Veuillez renseigner l'examinateur")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Some stuff to do when ok got clicked
                        }
                    })
                    .show();
        }
        else {
            String nom = txtNomExaminateur.getText().toString().replaceAll(" ", "-");

            try {
                if (nom.charAt(nom.length()-1) == '-') {
                    nom = nom.substring(0, nom.length() - 1);
                }
            }
            catch (Exception e)
            {
                System.out.println("Erreur : " + e);
            }
            idNomExaminateur = noms.indexOf(nom);

            if (idNomExaminateur == -1) {
                idNomExaminateur = noms.size()+1;
                Examinateur newExaminateur = new Examinateur(idNomExaminateur, nom);
                ok = newExaminateur.save(this);
            }
            else
            {
                idNomExaminateur++;
                ok=true;
            }

            if (ok == true) {

                String patho = txtPathologie.getText().toString();

                idPathologie = pathologies.indexOf(patho);

                if (idPathologie == -1) {
                    idPathologie = pathologies.size()+1;
                    DaoPathologie bdd = new DaoPathologie(this);
                    ok = bdd.insertPathologie(idPathologie,patho);
                    bdd.close();
                }
                else
                {
                    idPathologie++;
                    ok = true;
                }

                if (ok == true) {
                    nom = txtNom.getText().toString().replaceAll(" ", "-");
                    String prenom = txtPrenom.getText().toString().replaceAll(" ", "-");

                    try {
                        if (nom.charAt(nom.length()-1) == '-') {
                            nom = nom.substring(0, nom.length() - 1);
                        }

                        if (prenom.charAt(prenom.length()-1) == '-') {
                            prenom = prenom.substring(0, prenom.length() - 1);
                        }
                    }
                    catch (Exception e)
                    {
                        System.out.println("Erreur : " + e);
                    }

                    Patient patient = new Patient(-1, nom, prenom, txtBirthday.getText().toString(), txtPathologie.getText().toString(), idNomExaminateur);
                    idPatient = patient.save(this);
                    if(idPatient == -2)
                    {
                        new AlertDialog.Builder(this)
                                .setTitle("Alerte")
                                .setMessage("Une erreur s'est produit lors de l'enregistrement du nouveau patient, veuillez vérifier les données saisie et réésayer")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {}
                                })
                                .show();
                    } else {
                        Intent intentRepas = new Intent(newpatient.this, repas.class);
                        intentRepas.putExtra("Source", ""+idPatient); //ajout de "" devant idPatient car la valeur envoyée doit être un String

                        newpatient.this.finish();
                        newpatient.this.startActivity(intentRepas);
                    }
                }
            }
            else
            {
                new AlertDialog.Builder(this)
                        .setTitle("Alerte")
                        .setMessage("Une erreur s'est produit lors de l'enregistrement du nouvel examinateur, veuillez vérifier les données saisie et réésayer")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        })
                        .show();
            }
        }
    }
//endregion
}