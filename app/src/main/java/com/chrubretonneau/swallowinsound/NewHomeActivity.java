package com.chrubretonneau.swallowinsound;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExport;

import java.io.File;

public class NewHomeActivity extends Activity {

    private static final int PERMISSIONS_ALL = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
    };

    private ImageView analysis_btn;
    private ImageView list_analysis_btn;
    private ImageView food_btn;
    private ImageView patient_btn;
    private ImageView database_btn;
    private ImageView test_btn;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_new_home);
        this.setTitle(getResources().getString(R.string.short_app_name));

        checkPermissions();

        analysis_btn = findViewById(R.id.analysis_btn);
        list_analysis_btn = findViewById(R.id.list_analysis_btn);
        food_btn = findViewById(R.id.food_btn);
        patient_btn = findViewById(R.id.patient_btn);
        database_btn = findViewById(R.id.database_btn);
        test_btn = findViewById(R.id.tests_btn);

        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            showFinalAlert(getResources().getText(R.string.sdcard_readonly));
            return;
        }
        if (status.equals(Environment.MEDIA_SHARED)) {
            showFinalAlert(getResources().getText(R.string.sdcard_shared));
            return;
        }
        if (!status.equals(Environment.MEDIA_MOUNTED)) {
            showFinalAlert(getResources().getText(R.string.no_sdcard));
            return;
        }

        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound");
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    private void showFinalAlert(CharSequence message) {
        new AlertDialog.Builder(NewHomeActivity.this)
                .setTitle(getResources().getText(R.string.alert_title_failure))
                .setMessage(message)
                .setPositiveButton(
                        R.string.alert_ok_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                finish();
                            }
                        })
                .setCancelable(false)
                .show();
    }

    public void listAnalysisOnClick(View view) {
        Intent intent = new Intent(this, home.class);
        startActivity(intent);
    }

    public void analysisOnClick(View view) {
        Intent intentRepas = new Intent(this, repas.class);
        startActivity(intentRepas);
    }

    public void patientOnClick(View view) {
        Intent intentNewPatient = new Intent(this, newpatient.class);
        startActivity(intentNewPatient);
    }

    public void foodOnClick(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Gestion d'aliments")
                .setMessage("Cette fonctionnalité n'a pas encore été intégrée à l'application.")
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // Some stuff to do when cancel got clicked
                    }
                })
                .show();
    }


    public void testOnClick(View view) {
        Intent intentTest = new Intent(this, Test.class);
        startActivity(intentTest);
    }

    public void databaseOnClick(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Exportation")
                .setMessage("Exporter la base de données?")
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Export();
                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // Some stuff to do when cancel got clicked
                    }
                })
                .show();
    }

    private void Export() {
        DaoExport temp = new DaoExport(this);
        if (temp.export()) {
            Toast.makeText(this, "Export Terminé", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Echec de l'exportation!", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkPermissions() {
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_ALL);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_ALL) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{permissions[i]}, PERMISSIONS_ALL);
                }
            }
        }
    }
}
