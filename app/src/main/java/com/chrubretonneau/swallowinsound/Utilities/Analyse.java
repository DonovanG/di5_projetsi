package com.chrubretonneau.swallowinsound.Utilities;

import android.content.Context;

import com.chrubretonneau.swallowinsound.ModelesClass.Aliment;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoAliment;
import com.chrubretonneau.swallowinsound.ModelesClass.Deglutition;
import com.chrubretonneau.swallowinsound.R;
import com.chrubretonneau.swallowinsound.soundfile.Wav;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * @author Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class Analyse
{
//region getSwallows : récupère le nombre de déglutition dans un enregistrement Wav
    /**
     * La fonction getSwallows permet d'extraire les déglutitions (heure de début et heure de fin) d'un enregistrement audio wav.
     * @param path Correspond au chemin vers le dossier de l'examen visé, de type String
     * @param aliment Nom de l'aliment à traiter, de type String
     * @return Retourne une List de Deglutition contenant toute les déglutition extraite de l'enregistrement
     */
    static Vector<Double> data = new Vector<>();
    public static List<Deglutition> getSwallows(Context context, String path, Aliment aliment, boolean filtrePasse, boolean filtreAmp)
    {
        int x;
        int count = 0;
        int n = 100;
        int dividende = 0;

        if(aliment == null){
            throw new IllegalArgumentException("Aliment is null");
        }
        n = aliment.getParam1();
        dividende = aliment.getParam2();

        path = path + "/" + aliment.getLibelle() + ".wav";
        data = Wav.getAmplitude(context, path, filtrePasse);


        int i = 0;
        double min = 0;
        double max = 0;

        while(i<data.size())
        {
            if(data.get(i) < min)
            {
                min = data.get(i);
            }

            if(data.get(i) > max)
            {
                max = data.get(i);
            }

            i++;
        }

        double condition = 0;
        if(filtreAmp) {
            condition = max * (double) context.getResources().getInteger(R.integer.filter) / 100.0;//condition de filtre
        }

        min = min / dividende;
        max = max / dividende;

        try
        {
            List<Deglutition> deglutitions = new ArrayList<Deglutition>();
            int[] deglu = new int[2];
            deglu[0] = -1;

            boolean filtre = false;
            for(x = 0; x < data.size(); x++)
            {
                if(data.get(x) >= condition)
                {
                    filtre = true;
                }
                if((data.get(x) <= min || data.get(x) >= max) && deglu[0] == -1) //Au delà des 2 seuils et début deglutition KO
                {
                    deglu[0] = x; //Début déglutition à l'index x
                    filtre = false;
                    if(data.get(x) >= condition)
                    {
                        filtre = true;
                    }
                }
                else if ((data.get(x) <= min || data.get(x) >= max) && deglu[0] != -1) //Au delà des 2 seuil et début déglutitition OK
                {
                    deglu[1] = x; //Fin de deglutition temporaire
                    count = 0;
                }
                else if((data.get(x) > min || data.get(x) < max) && deglu[0] != -1)  //Entre les 2 seuils et début de déglutition OK
                {
                    if(count == 275) //temps minimal pendant lequel l'amplitude reste entre les 2 seuils pour qu'une déglutition soit considéré comme terminé
                    {
                        if(deglu[1] - deglu[0] > n && filtre) //Vérifie que la durée de la déglutition trouvé est bien supérieure à la durée minimal d'une déglutition (n variant selon aliment, voir début de fonction)
                        {
                            Deglutition deglutition = new Deglutition(deglu[0], deglu[1]);
                            deglutitions.add(deglutition);
                        }

                        deglu[0] = -1;
                        deglu[1] = -1;
                        count = 0;
                    }
                    else
                    {
                        count++;
                    }
                }
            }

            return deglutitions;
        }
        catch (Exception e)
        {
            System.err.println(e);
            return null;
        }
    }
//endregion

    public static List<Deglutition> getSwallows(Context context, String path, Aliment aliment) { return getSwallows(context,path,aliment,false,false); }


//region getStatistics
    /**
     * La fonction getStatistics permet d'extraire les statistique Durees totale et pas aliment, nombres de déglutions total et par aliment, durées moyennes de déglutitions totale et par aliment et durées moyennes inter-déglutition totale et par aliment.
     * @param dureesMS Durées de chaque aliment, de type long[]
     * @param deglutitions Liste des listes des déglutitions de chaque aliments, de type List<List<Deglutition>>
     * @return Retourne une HashMap contenant les statistiques suivantes : <br/>
     * Key : Durees ; Value : de type String[4] <br/>
     * Key : NbDeglus ; Value : de type Int[4] <br/>
     * Key : DureeMoyenneDeglu ; Value : de type String[4] <br/>
     * Key : DureeMoyenneInterDeglu ; Value : de type String[4] <br/>
     */
    public static HashMap<String, Object> getStatistics(long[] dureesMS, List<List<Deglutition>> deglutitions)
    {
        int min, sec, ms;
        String duree;
        String[] durees = new String[4];
        long repas, dureePuree, dureeEau, dureeYaourt;
        int[] nbDeglus = new int[4];
        nbDeglus[0] = 0;
        double dureeMoyenne=0;
        double dureeMoyenneDegluTT, dureeMoyenneInterTT;
        String[] dureeMoyenneDeglu = new String[4];
        String[] dureeMoyenneInter = new String[4];



        dureePuree = dureesMS[0];
        dureeEau = dureesMS[1];
        dureeYaourt = dureesMS[2];
        repas = dureePuree + dureeEau + dureeYaourt;

        /*------------------ Conversion format Duree Repas ------------------*/
        min = (int) (repas / 60000);
        sec = (int) (repas - min * 60000) / 1000;
        ms = (int) (repas - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }
        durees[0] = duree;
        /*-------------------------------------------------------------------*/

        /*------------------ Conversion format Duree Puree ------------------*/
        min = (int) (dureePuree / 60000);
        sec = (int) (dureePuree - min * 60000) / 1000;
        ms = (int) (dureePuree - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }
        durees[1] = duree;
        /*-------------------------------------------------------------------*/

        /*------------------- Conversion format Duree Eau -------------------*/
        min = (int) (dureeEau / 60000);
        sec = (int) (dureeEau - min * 60000) / 1000;
        ms = (int) (dureeEau - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }
        durees[2] = duree;
        /*-------------------------------------------------------------------*/

        /*------------------ Conversion format Duree Yaourt ------------------*/
        min = (int) (dureeYaourt / 60000);
        sec = (int) (dureeYaourt - min * 60000) / 1000;
        ms = (int) (dureeYaourt - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }
        durees[3] = duree;
        /*--------------------------------------------------------------------*/

        List<Deglutition> listeDeglutitionsPuree = deglutitions.get(0);
        List<Deglutition> listeDeglutitionsEau = deglutitions.get(1);
        List<Deglutition> listeDeglutitionsYaourt = deglutitions.get(2);

        //----------------------------------- Statistiques Puree ----------------------------------
        nbDeglus[1] = listeDeglutitionsPuree.size();

        /*----------------------------- Duree Deglutition Moyenne DDM -----------------------------*/
        for(int i = 0; i<listeDeglutitionsPuree.size(); i++) {
            Deglutition deglu = listeDeglutitionsPuree.get(i);

            dureeMoyenne += (deglu.getTimeEndDeglutition() - deglu.getTimeBeginDeglutition());
        }

        dureeMoyenneDegluTT = dureeMoyenne;

        dureeMoyenne = dureeMoyenne / nbDeglus[1];
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneDeglu[1] = duree;
        /*------------------------------------------------------------------------------------------*/

        /*-------------------------- Duree Inter-Deglutition Moyenne DIDM --------------------------*/
        dureeMoyenne=0;
        for(int i = 1; i<listeDeglutitionsPuree.size(); i++) {
            Deglutition degluPrec = listeDeglutitionsPuree.get(i-1);
            Deglutition deglu = listeDeglutitionsPuree.get(i);

            dureeMoyenne += (deglu.getTimeBeginDeglutition() - degluPrec.getTimeEndDeglutition());
        }


        dureeMoyenneInterTT = dureeMoyenne;

        dureeMoyenne = dureeMoyenne / (nbDeglus[1]-1);
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }


        dureeMoyenneInter[1] = duree;
        //-----------------------------------------------------------------------------------------


        //------------------------------------ Statistiques Eau -----------------------------------
        nbDeglus[2] = listeDeglutitionsEau.size();

        /*----------------------------- Duree Deglutition Moyenne DDM -----------------------------*/
        dureeMoyenne=0;
        for(int i = 0; i<listeDeglutitionsEau.size(); i++) {
            Deglutition deglu = listeDeglutitionsEau.get(i);

            dureeMoyenne += (deglu.getTimeEndDeglutition() - deglu.getTimeBeginDeglutition());
        }

        dureeMoyenneDegluTT += dureeMoyenne;

        dureeMoyenne = dureeMoyenne / nbDeglus[2];
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }


        dureeMoyenneDeglu[2] = duree;
        /*------------------------------------------------------------------------------------------*/

        /*-------------------------- Duree Inter-Deglutition Moyenne DIDM --------------------------*/
        dureeMoyenne=0;
        for(int i = 1; i<listeDeglutitionsEau.size(); i++) {
            Deglutition degluPrec = listeDeglutitionsEau.get(i-1);
            Deglutition deglu = listeDeglutitionsEau.get(i);

            dureeMoyenne += (deglu.getTimeBeginDeglutition() - degluPrec.getTimeEndDeglutition());
        }

        dureeMoyenneInterTT += dureeMoyenne;

        dureeMoyenne = dureeMoyenne / (nbDeglus[2]-1);
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneInter[2] = duree;
        //-----------------------------------------------------------------------------------------


        //----------------------------------- Statistiques Yaourt ----------------------------------
        nbDeglus[3] = listeDeglutitionsYaourt.size();

        /*----------------------------- Duree Deglutition Moyenne DDM -----------------------------*/
        dureeMoyenne=0;
        for(int i = 0; i<listeDeglutitionsYaourt.size(); i++) {
            Deglutition deglu = listeDeglutitionsYaourt.get(i);

            dureeMoyenne += (deglu.getTimeEndDeglutition() - deglu.getTimeBeginDeglutition());
        }

        dureeMoyenneDegluTT += dureeMoyenne;

        dureeMoyenne = dureeMoyenne / nbDeglus[3];
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneDeglu[3] = duree;
        /*------------------------------------------------------------------------------------------*/

        /*-------------------------- Duree Inter-Deglutition Moyenne DIDM --------------------------*/
        dureeMoyenne=0;
        for(int i = 1; i<listeDeglutitionsYaourt.size(); i++) {
            Deglutition degluPrec = listeDeglutitionsYaourt.get(i-1);
            Deglutition deglu = listeDeglutitionsYaourt.get(i);

            dureeMoyenne += (deglu.getTimeBeginDeglutition() - degluPrec.getTimeEndDeglutition());
        }

        dureeMoyenneInterTT += dureeMoyenne;

        dureeMoyenne = dureeMoyenne / (nbDeglus[3]-1);
        min = (int) (dureeMoyenne / 60000);
        sec = (int) (dureeMoyenne - min * 60000) / 1000;
        ms = (int) (dureeMoyenne - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneInter[3] = duree;
        //------------------------------------------------------------------------------------------

        //----------------------------------- Statistiques Globale ----------------------------------

        nbDeglus[0] = nbDeglus[1]+nbDeglus[2]+nbDeglus[3];

        /*------------------------ Duree Deglutition Moyenne Totale DDMTT -------------------------*/
        dureeMoyenneDegluTT = dureeMoyenneDegluTT / nbDeglus[0];
        min = (int) (dureeMoyenneDegluTT / 60000);
        sec = (int) (dureeMoyenneDegluTT - min * 60000) / 1000;
        ms = (int) (dureeMoyenneDegluTT - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneDeglu[0] = duree;
        /*-----------------------------------------------------------------------------------------*/

        /*--------------------- Duree Inter-Deglutition Moyenne Totale DIDMTT ---------------------*/
        int antiBug = 0;//Au cas ou il n'y ait pas d'interdeglutition dans un fichier;
        if(nbDeglus[1] > 0)
            antiBug++;
        if(nbDeglus[2] > 0)
            antiBug++;
        if(nbDeglus[3] > 0)
            antiBug++;
        dureeMoyenneInterTT = dureeMoyenneInterTT / (nbDeglus[0] - antiBug);
        min = (int) (dureeMoyenneInterTT / 60000);
        sec = (int) (dureeMoyenneInterTT - min * 60000) / 1000;
        ms = (int) (dureeMoyenneInterTT - min * 60000 - sec * 1000);
        if(min == 0 && sec == 0)
        {
            duree = ms + "ms";
        }
        else if(min == 0)
        {
            duree = sec + " sec " + ms + "ms";
        }
        else {
            duree = min + " min " + sec + " sec " + ms + "ms";
        }

        dureeMoyenneInter[0] = duree;

        //------------------------------------------------------------------------------------------

        HashMap<String, Object> statistiques = new HashMap<String, Object>();
        statistiques.put("Durees", durees);
        statistiques.put("NbDeglus", nbDeglus);
        statistiques.put("DureeMoyenneDeglu", dureeMoyenneDeglu);
        statistiques.put("DureeMoyenneInterDeglu", dureeMoyenneInter);


        return statistiques;
    }
//endregion
}
