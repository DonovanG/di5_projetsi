package com.chrubretonneau.swallowinsound.Utilities;

public class Filter {
    private double a1;
    private double a2;
    private double a3;
    private double b1;
    private double b2;

    /// <summary>
    /// Array of input values, latest are in front
    /// </summary>
    private double[] inputHistory = new double[2];

    /// <summary>
    /// Array of output values, latest are in front
    /// </summary>
    private double[] outputHistory = new double[3];

	/// resonance amount, from sqrt(2) to ~ 0.1
    public Filter(double frequency, int sampleRate, PassType passType, double resonance)
    {

	    double c;

        switch (passType)
        {
            case LowPass:
                c = 1.0f / (double) Math.tan( Math.PI * frequency / sampleRate );
                a1 = 1.0f / ( 1.0f + resonance * c + c * c );
                a2 = 2f * a1;
                a3 = a1;
                b1 = 2.0f * ( 1.0f - c * c ) * a1;
                b2 = (1.0f - resonance * c + c * c ) * a1;
                break;

            case HighPass:
                c = (double)Math.tan( Math.PI * frequency / sampleRate );
                a1 = 1.0f / ( 1.0f + resonance * c + c * c );
                a2 = -2f * a1;
                a3 = a1;
                b1 = 2.0f * ( c * c - 1.0f) * a1;
                b2 = (1.0f - resonance * c + c * c ) * a1;
                break;
        }

        inputHistory[0] = 0;
        inputHistory[1] = 0;
        outputHistory[0] = 0;
        outputHistory[1] = 0;
        outputHistory[2] = 0;
    }

    public enum PassType
    {
	    HighPass,
	    LowPass,
    }

    public void Update(double newInput)
    {
        double newOutput = a1 * newInput + a2 * this.inputHistory[0] + a3 * this.inputHistory[1] - b1 * this.outputHistory[0] - b2 * this.outputHistory[1];

        this.inputHistory[1] = this.inputHistory[0];
        this.inputHistory[0] = newInput;

        this.outputHistory[2] = this.outputHistory[1];
        this.outputHistory[1] = this.outputHistory[0];
        this.outputHistory[0] = newOutput;
    }


    public double getValue()
    {
        return this.outputHistory[0];
    }

}