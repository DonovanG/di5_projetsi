package com.chrubretonneau.swallowinsound;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 *author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class ExamensAdapter extends BaseAdapter
{
    // Une liste d'examen détaillés
    private List<List<String>> listeExamens;

    //Le contexte dans lequel est présent notre adapter
    private Context context;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater inflater;


    public ExamensAdapter(Context unContext, List<List<String>> uneListeExamens) {
        context = unContext;
        listeExamens = uneListeExamens;
        inflater = LayoutInflater.from(unContext);
    }

    @Override
    public int getCount() {
        return listeExamens.size();
    }

    @Override
    public Object getItem(int i) {
        return listeExamens.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem;
        //(1) : Réutilisation des layouts
        if (convertView == null) {
            //Initialisation de notre item à partir du  layout XML "layout_itemexamen.xml"
            layoutItem = (LinearLayout) inflater.inflate(R.layout.layout_itemexamen, parent, false);
        } else {
            layoutItem = (LinearLayout) convertView;
        }

        //(2) : Récupération des TextView de notre layout
        TextView txtIdExamen = (TextView)layoutItem.findViewById(R.id.row_idExamen);
        TextView txtPatient = (TextView)layoutItem.findViewById(R.id.row_patient);
        TextView txttxtDate = (TextView)layoutItem.findViewById(R.id.row_date);
        TextView txtExaminateur = (TextView)layoutItem.findViewById(R.id.row_examinateur);
        TextView txtDuree = (TextView)layoutItem.findViewById(R.id.row_duree);

        //(3) : Renseignement des valeurs
        txtIdExamen.setText(listeExamens.get(position).get(0));
        txtPatient.setText(listeExamens.get(position).get(1)+"_"+listeExamens.get(position).get(2));
        txttxtDate.setText(listeExamens.get(position).get(3));
        txtExaminateur.setText(listeExamens.get(position).get(4)+"_"+listeExamens.get(position).get(5));
        txtDuree.setText(listeExamens.get(position).get(6));

        //On retourne l'item créé.
        return layoutItem;
    }
}
