package com.chrubretonneau.swallowinsound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.Spinner;
import android.widget.Toast;

import com.chrubretonneau.swallowinsound.ModelesClass.Aliment;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoDeglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoPatient;
import com.chrubretonneau.swallowinsound.Utilities.Analyse;
import com.chrubretonneau.swallowinsound.ModelesClass.Deglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Examen;
import com.chrubretonneau.swallowinsound.ModelesClass.Examinateur;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;
import com.chrubretonneau.swallowinsound.soundfile.Wav;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class repas extends Activity
{
//region Variables
    private Spinner spPatient, spPuree, spEau, spYaourt;
    private Button btStart, btStop, recPuree, recEau, recYaourt, ckPuree, ckEau, ckYaourt;
    private Chronometer chronoAliment;
    private CheckBox checkBoxPass, checkBoxAmp;
    private File path;
    private Wav.Record record = new Wav.Record();
    private MediaPlayer test;
    private String dateAndHour;
    private int compteurAliment = 1;
    private int mode; //0 = Insert ; 1 = Update
// endregion

//region onCreate : chargement de la liste des patient dans liste déroulante et si page précédente addPatient on place la liste déroulante sur le patient précédemment ajouté
    /**
     * Au chargement de l'activité on charge les différents spinner contenant respectivement le nom des Patients et les quantités d'aliments à ingérer. <br/>
     * Si l'activité précédente est celle permetant l'enregistrement d'un nouveau patient, on place le spinner contenant les noms des patients directement sur celui venant d'être enregistré.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repas);

        spPatient = (Spinner) findViewById(R.id.spPatient);
        spPuree = (Spinner) findViewById(R.id.spPuree);
        spEau = (Spinner) findViewById(R.id.spEau);
        spYaourt = (Spinner) findViewById(R.id.spYaourt);
        btStart = (Button) findViewById(R.id.btStart);
        btStop = (Button) findViewById(R.id.btStop);
        recPuree = (Button) findViewById(R.id.recPuree);
        recEau = (Button) findViewById(R.id.recEau);
        recYaourt = (Button) findViewById(R.id.recYaourt);
        ckPuree = (Button) findViewById(R.id.ckPuree);
        ckEau = (Button) findViewById(R.id.ckEau);
        ckYaourt = (Button) findViewById(R.id.ckYaourt);
        chronoAliment = (Chronometer) findViewById(R.id.dureeRepas);
        checkBoxPass = (CheckBox) findViewById(R.id.chkBoxPass);
        checkBoxAmp = (CheckBox) findViewById(R.id.chkBoxAmp);


        try {
            int indexPatient = -1;
            spPatient = (Spinner) findViewById(R.id.spPatient);
            DaoPatient bdd = new DaoPatient(this);
            List<String> patients = bdd.getAllPatronymePatient();
            bdd.close();

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String selectedPatient = patients.get(Integer.parseInt(extras.getString("Source")) - 1);
                Collections.sort(patients);

                int i = 0;
                while (indexPatient == -1 || i < patients.size()) {
                    if (patients.get(i).equals(selectedPatient)) {
                        indexPatient = i;
                    }
                    i++;
                }
            } else {
                Collections.sort(patients);
            }

            // Creating adapter for spinner
            if (!patients.get(0).equals(""))
                patients.add(0, "");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, patients);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            spPatient.setAdapter(dataAdapter);
            if (indexPatient != -1) {
                spPatient.setSelection(indexPatient);
                path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + spPatient.getSelectedItem().toString() + "/" + dateAndHour);
            }

            Calendar c = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH");
            dateAndHour = df.format(c.getTime()) + "h";

        }
        catch (Exception ex)
        {
            System.out.println("Erreur : " + ex);
        }

        spPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                try {
                    path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + spPatient.getSelectedItem().toString() + "/" + dateAndHour);

                    if(path.exists() && new File(path.getPath() + "/Puree.wav").exists())
                    {
                        new AlertDialog.Builder(repas.this)
                                .setTitle("Alerte")
                                .setMessage("Un examen a déjà été réalisé à cette date et heure, continuer effacera les données précédentes")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        mode = 1;
                                    }
                                })
                                .show();
                    }
                    else
                    {
                        mode = 0;
                    }
                }
                catch (Exception e)
                {
                    System.out.println("erreur : " + e);
                }
            }

            public void onNothingSelected(AdapterView<?> parent){

            }
        });
    }
//endregion

    /**
     * La fonction btAddPatientOnClick correspond à l'action suivant le clic sur le bouton "AddPattient". <br/>
     * Cette fonction permet de basculer vers la page d'enregistrement d'un nouveau patient si ce dernier n'est pas présent dans la liste.
     * @param view
     */
    public void btAddPatientOnClick(View view) {
        Intent intentNewPatient = new Intent(this, newpatient.class);
        this.startActivity(intentNewPatient);
    }

//region Test
    /**
     * La fonction btTestRecordOnClick correspond à l'action suivant le clic sur le bouton "Tester le micro". <br/>
     * Cette fonction permet d'enregistrer un test de bon fonctionnement et placement du laryngophone.
     * @param view
     */
    public void btTestRecordOnClick(View view)
    {
        if(spPatient.getAdapter().getCount() == 0)
        {
            new AlertDialog.Builder(this)
                    .setTitle("Alerte")
                    .setMessage("Aucun patient n'a été sélectionné, souhaitez-vous basculer sur la page d'enregistrement d'un nouveau patient ?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            try
                            {
                                Intent intentNewPatient = new Intent(repas.super.getBaseContext(), newpatient.class);
                                startActivity(intentNewPatient);
                            } catch (Exception e) {
                                System.out.println("test() failed : " + e.toString());
                            }
                        }
                    })
                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Some stuff to do when cancel got clicked
                        }
                    })
                    .show();
        }
        else
        {
            if (!path.exists()) {
                path.mkdirs();
            }

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

            new AlertDialog.Builder(this)
                    .setTitle("Test")
                    .setMessage("Le test de votre laryngophone durera 15 secondes après validation!")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                Button btPlay = (Button) findViewById(R.id.btTestPlay);
                                record.init(path.getPath(), "Test");
                                record.startRecording();
                                Thread.sleep(15000);
                                record.stopRecording();

                                btPlay.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                System.out.println("test() failed : " + e.toString());
                            }
                        }
                    })
                    .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Some stuff to do when cancel got clicked
                        }
                    })
                    .show();
        }
    }

    /**
     * La fonction btTestPlayOnClick correspond à l'action suivant le clic sur le bouton "TestPlay". <br/>
     * Cette fonction permet de lire le test de bon fonctionnement du laryngophone enregistrer.
     * @param view
     */
    public void btTestPlayOnClick(View view)
    {
        Button btStart = (Button) findViewById(R.id.btTestPlay);
        Button btStop = (Button) findViewById(R.id.btTestStop);
        try
        {
            Uri file = Uri.parse(path.getPath() + "/Test.wav");
            test = MediaPlayer.create(this, file);
            test.start();

            btStart.setVisibility(View.GONE);
            btStop.setVisibility(View.VISIBLE);
        }
        catch (Exception e)
        {
            System.out.println("test() failed : " + e.toString());
        }
    }

    /**
     * La fonction btTestStopOnClick correspond à l'action suivant le clic sur le bouton "TestStop". <br/>
     * Cette fonction permet de stopper la lecture du test de bon fonctionnement du laryngophone enregistrer.
     * @param view
     */
    public void btTestStopOnClick(View view)
    {
        Button btStart = (Button) findViewById(R.id.btTestPlay);
        Button btStop = (Button) findViewById(R.id.btTestStop);

        try
        {
            test.stop();

            btStop.setVisibility(View.GONE);
            btStart.setVisibility(View.VISIBLE);
        }
        catch (Exception e)
        {
            System.out.println("test() failed : " + e.toString());
        }
    }
//endregion

//region btStart : On désactive les spinners et le bouton start et on active le bouton next puis on démarre l'enregistrement du 1er aliment
    /**
     * La fonction btStartOnClick correspond à l'action suivant le clic sur le bouton "Play". <br/>
     * Cette fonction permet de démarrer l'enregistrement audio et également le chronomètre.
     * @param view
     */
    public void btStartOnClick(View view)
    {
        if (spPatient.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "You need to select a patient before", Toast.LENGTH_SHORT).show();
            return;
        }
        try
        {
            switch (compteurAliment){
                case 1:
                    if(spPatient.getAdapter().getCount() == 0)
                    {
                        new AlertDialog.Builder(this)
                                .setTitle("Alerte")
                                .setMessage("Aucun patient n'a été sélectionné, souhaitez-vous basculer sur la page d'enregistrement d'un nouveau patient ?")
                                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        try
                                        {
                                            Intent intentNewPatient = new Intent(repas.super.getBaseContext(), newpatient.class);
                                            startActivity(intentNewPatient);
                                        } catch (Exception e) {
                                            System.out.println("test() failed : " + e.toString());
                                        }
                                    }
                                })
                                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Some stuff to do when cancel got clicked
                                    }
                                })
                                .show();
                    }
                    else {
                        try {
                            btStart.setVisibility(View.INVISIBLE);
                            btStop.setVisibility(View.VISIBLE);

                            if (!path.exists())
                            {
                                path.mkdirs();
                            }
                            else
                            {
                                if(new File(path.getPath() + "/Puree.wav").exists())
                                {
                                    new File(path.getPath() + "/Puree.wav").delete();
                                    new File(path.getPath() + "/Eau.wav").delete();
                                    new File(path.getPath() + "/Yaourt.wav").delete();
                                }
                            }

                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                            spPatient.setEnabled(false);
                            spPuree.setEnabled(false);
                            spEau.setEnabled(false);
                            spYaourt.setEnabled(false);

                            recPuree.setVisibility(View.VISIBLE);

                            record.init(path.getPath(), "Puree");

                            chronoAliment.setBase(SystemClock.elapsedRealtime());
                            chronoAliment.start();
                            record.startRecording();
                        } catch (Exception e) {
                            System.out.println("reset() failed : " + e.toString());
                        }
                    }
                    break;

                case 2:
                    try
                    {
                        btStart.setVisibility(View.INVISIBLE);
                        btStop.setVisibility(View.VISIBLE);

                        recEau.setVisibility(View.VISIBLE);

                        record.init(path.getPath(), "Eau");

                        chronoAliment.setBase(SystemClock.elapsedRealtime());
                        chronoAliment.start();
                        record.startRecording();
                    }
                    catch (Exception e)
                    {
                        System.out.println("reset() failed : " + e.toString());
                    }
                    break;

                case 3:
                    try
                    {
                        btStart.setVisibility(View.INVISIBLE);
                        btStop.setVisibility(View.VISIBLE);

                        recYaourt.setVisibility(View.VISIBLE);

                        record.init(path.getPath(), "Yaourt");

                        chronoAliment.setBase(SystemClock.elapsedRealtime());
                        chronoAliment.start();
                        record.startRecording();
                    }
                    catch (Exception e)
                    {
                        System.out.println("reset() failed : " + e.toString());
                    }
                    break;
            }
        }
        catch(Exception e2)
        {
            System.out.println("failed : " + e2.toString());
        }
    }
//endregion

//region btStop : On stop l'enregistrement, on check l'aliment à l'écran ; Au 3ème aliment on enregistre l'examen dans la base puis on commence l'analyse
    /**
     * La fonction btStopOnClick correspond à l'action suivant le clic sur le bouton "Stop". <br/>
     * Cette fonction permet de d'arrêter l'enregistrement audio et également le chronomètre. <br/>
     * On enregistre chaque temps et sur le "Stop" du dernier aliment on enregistre l'exmamen dans la base de données, on calcule les différentes déglutitions que l'on enregistre également dans la base de données. <br/>
     * Enfin on bascule sur la page du rapport.
     * @param view
     */
    public void btStopOnClick(View view) {
        try {
            btStop.setVisibility(View.INVISIBLE);
            btStart.setVisibility(View.VISIBLE);

            record.stopRecording();

            chronoAliment.stop();

            switch (compteurAliment)
            {
                case 1:

                    recPuree.setVisibility(View.INVISIBLE);
                    ckPuree.setVisibility(View.VISIBLE);
                    compteurAliment++;

                    break;
                case 2:

                    recEau.setVisibility(View.INVISIBLE);
                    ckEau.setVisibility(View.VISIBLE);
                    compteurAliment++;

                    break;
                case 3:

                    recYaourt.setVisibility(View.INVISIBLE);
                    ckYaourt.setVisibility(View.VISIBLE);


                    new AlertDialog.Builder(this)
                            .setTitle("Analyse")
                            .setMessage("Examen terminé, veuillez patienter le temps que l'examen soit analysé, vous basculerez sur la page du rapport global lorsque celle-ci sera terminée.")
                            .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    File file = new File(path.getPath() + "/Test.wav");
                                    file.delete();

                                    HashMap<String, Object> data = save();

                                    Intent intentRapport = new Intent(repas.super.getBaseContext(), rapportGlobal.class);
                                    intentRapport.putExtra("Examen", data);
                                    repas.this.finish();
                                    startActivity(intentRapport);
                                }
                            })
                            .setCancelable(false)
                            .show();



                            /*HashMap<String, Object> data = save();

                            Intent intentRapport = new Intent(repas.super.getBaseContext(), rapportGlobal.class);
                            intentRapport.putExtra("Examen", data);
                            repas.this.finish();
                            startActivity(intentRapport);*/
                    break;
            }

            chronoAliment.setBase(SystemClock.elapsedRealtime());
        }
        catch (Exception e)
        {
            System.out.println("failed : " +e);
        }
    }


    /**
     *La fonction save fait appelle aux fonctions Analyse.getSwallow et Analyse.getStatisticsafin afin d'extraire les déglutitons des fichiers wav et récupérer les différentes statistiques.
     * @return Retourne une HashMap<String, Object> contenant  : <br/>
     * Key : Patient ; Value : le patient de type Patient <br/>
     * Key : Examen ; Value : l'examen de type Examen <br/>
     * Key : Examinateur ; Value : le nom de l'examinateur de type String <br/>
     */
    private HashMap<String, Object> save()
    {
        long[] dureesMS = new long[3];
        List<List<Deglutition>> listesDeglutitions = new ArrayList<>();

        Patient patient = new Patient(spPatient.getSelectedItem().toString(), repas.this);
        Examinateur examinateur = new Examinateur(patient.getIdExaminateur(), repas.this);
        String[] quantitees = {spPuree.getSelectedItem().toString(), spEau.getSelectedItem().toString(), spYaourt.getSelectedItem().toString()};

        List<Deglutition> listeDeglutitionsPuree = Analyse.getSwallows(this, path.getPath(), new Aliment(1,"Puree",125,13), checkBoxPass.isChecked(), checkBoxAmp.isChecked());
        List<Deglutition> listeDeglutitionsEau = Analyse.getSwallows(this, path.getPath(), new Aliment(1,"Eau",75,13), checkBoxPass.isChecked(), checkBoxAmp.isChecked());
        List<Deglutition> listeDeglutitionsYaourt = Analyse.getSwallows(this, path.getPath(), new Aliment(1,"Yaourt",75,9), checkBoxPass.isChecked(), checkBoxAmp.isChecked());

        if(!listeDeglutitionsPuree.isEmpty()) {
            dureesMS[0] = (long) (listeDeglutitionsPuree.get(listeDeglutitionsPuree.size() - 1).getTimeEndDeglutition() - listeDeglutitionsPuree.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[0] = 0;
        }

        if(!listeDeglutitionsEau.isEmpty()) {
            dureesMS[1] = (long) (listeDeglutitionsEau.get(listeDeglutitionsEau.size() - 1).getTimeEndDeglutition() - listeDeglutitionsEau.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[1] = 0;
        }

        if(!listeDeglutitionsYaourt.isEmpty()) {
            dureesMS[2] = (long) (listeDeglutitionsYaourt.get(listeDeglutitionsYaourt.size() - 1).getTimeEndDeglutition() - listeDeglutitionsYaourt.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[2] = 0;
        }


        listesDeglutitions.add(listeDeglutitionsPuree);
        listesDeglutitions.add(listeDeglutitionsEau);
        listesDeglutitions.add(listeDeglutitionsYaourt);


        HashMap<String, Object> statistiques = Analyse.getStatistics(dureesMS, listesDeglutitions);
        Examen examen = new Examen(patient.getIdPatient(), dateAndHour, quantitees, (String[]) statistiques.get("Durees"), (int[]) statistiques.get("NbDeglus"), (String[]) statistiques.get("DureeMoyenneDeglu"), (String[]) statistiques.get("DureeMoyenneInterDeglu"), this);



        HashMap<String, String> dataForHomePage = new HashMap<String, String>();
        dataForHomePage.put("Patient", patient.getPrenom() + " " + patient.getNom());
        dataForHomePage.put("Date", dateAndHour);
        dataForHomePage.put("Examinateur", examinateur.getNom());
        dataForHomePage.put("Duree", examen.getDurees()[0]);

        int idExamen;
        if (mode == 0) {
            idExamen = examen.save(this);
            examen.setIdExamen(idExamen);

            dataForHomePage.put("idExamen", idExamen + "");
            home.examens.add(0, dataForHomePage);
            home.adapter.notifyDataSetChanged();
        } else {
            idExamen = examen.update(this);
            examen.setIdExamen(idExamen);

            DaoDeglutition bdd = new DaoDeglutition(this);

            bdd.deleteDeglutition(idExamen);
        }

        HashMap<String, Object> dataForRapport = new HashMap<String, Object>();
        dataForRapport.put("Patient", patient);
        dataForRapport.put("Examen", examen);
        dataForRapport.put("Examinateur", examinateur.getNom());



        for (Deglutition deglu : listeDeglutitionsPuree) {
            deglu.save(idExamen, 1, this);
        }
        for (Deglutition deglu : listeDeglutitionsEau) {
            deglu.save(idExamen, 2, this);
        }
        for (Deglutition deglu : listeDeglutitionsYaourt) {
            deglu.save(idExamen, 3, this);
        }


        //end = true;
        return dataForRapport;
    }
//endregion
}
