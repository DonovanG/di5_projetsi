package com.chrubretonneau.swallowinsound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.HashMap;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExamen;
import com.chrubretonneau.swallowinsound.ModelesClass.Examen;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;

/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
*/

public class rapportGlobal extends Activity {

    private HashMap<String, Object> data = new HashMap<String, Object>();


    @Override
    protected void onResume() {
        super.onResume();
        updateDisplay();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateDisplay();

    }

    private void updateDisplay() {


        DaoExamen daoExamen = new DaoExamen(this);
        examen = daoExamen.getExamen(examen.getIdExamen());

        data.remove("examen");
        data.put("Examen", examen);


        lblDureeTT.setText("Durée totale du repas : " + examen.getDurees()[0]);
        lblNbDegluTT.setText("Nombre Total de déglutitions : " + examen.getNbDeglu()[0]);
        lblDureeMoyenneDegluTT.setText("DDM Totale : " + examen.getDureesMoyenneDeglu()[0]);
        lblDureeMoyenneInterDegluTT.setText("DIDM Totale : " + examen.getDureesMoyenneInter()[0]);

        lblDureePuree.setText("Durée : " +examen.getDurees()[1]);
        lblNbDegluPuree.setText("Nombre de déglutitions : " + examen.getNbDeglu()[1]);
        lblDureeMoyenneDegluPuree.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[1]);
        lblDureeMoyenneInterDegluPuree.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[1]);

        lblDureeEau.setText("Durée : " +examen.getDurees()[2]);
        lblNbDegluEau.setText("Nombre de déglutitions : " + examen.getNbDeglu()[2]);
        lblDureeMoyenneDegluEau.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[2]);
        lblDureeMoyenneInterDegluEau.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[2]);

        lblDureeYaourt.setText("Durée : " +examen.getDurees()[3]);
        lblNbDegluYaourt.setText("Nombre de déglutitions : " + examen.getNbDeglu()[3]);
        lblDureeMoyenneDegluYaourt.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[3]);
        lblDureeMoyenneInterDegluYaourt.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[3]);

    }


    /**
     * A la création de l'activité on affiche toutes les données patient et statistique de l'examen via l'intent.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rapport_global);


        data = (HashMap<String, Object>) getIntent().getSerializableExtra("Examen");
        if(data.size() == 3)
        {
            patient = (Patient) data.get("Patient");
            examen = (Examen) data.get("Examen");
        }
        else
        {
            patient = new Patient((String) data.get("Patient"), this);
            examen = new Examen(Integer.parseInt((String) data.get("idExamen")), this);
            String examinateur = data.get("Examinateur").toString();
            data.clear();
            data.put("Patient", patient);
            data.put("Examen", examen);
            data.put("Examinateur", examinateur);
        }


        lblPatient = (TextView) this.findViewById(R.id.lblPatient);
        lblExaminateur = (TextView) this.findViewById(R.id.lblExaminateur);
        lblExamen = (TextView) this.findViewById(R.id.lblExamin);

        lblDureeTT = (TextView) this.findViewById(R.id.lblDureeTT);
        lblNbDegluTT = (TextView) this.findViewById(R.id.lblNbDegluTT);
        lblDureeMoyenneDegluTT = (TextView) this.findViewById(R.id.lblDureeMoyenneDegluTT);
        lblDureeMoyenneInterDegluTT = (TextView) this.findViewById(R.id.lblDureeMoyenneInterDegluTT);

        lblQuantitePuree = (TextView) this.findViewById(R.id.lblQuantitePuree);
        lblDureePuree = (TextView) this.findViewById(R.id.lblDureePuree);
        lblNbDegluPuree = (TextView) this.findViewById(R.id.lblNbDegluPuree);
        lblDureeMoyenneDegluPuree = (TextView) this.findViewById(R.id.lblDureeMoyenneDegluPuree);
        lblDureeMoyenneInterDegluPuree = (TextView) this.findViewById(R.id.lblDureeMoyenneInterDegluPuree);

        lblQuantiteEau = (TextView) this.findViewById(R.id.lblQuantiteEau);
        lblDureeEau = (TextView) this.findViewById(R.id.lblDureeEau);
        lblNbDegluEau = (TextView) this.findViewById(R.id.lblNbDegluEau);
        lblDureeMoyenneDegluEau = (TextView) this.findViewById(R.id.lblDureeMoyenneDegluEau);
        lblDureeMoyenneInterDegluEau = (TextView) this.findViewById(R.id.lblDureeMoyenneInterDegluEau);

        lblQuantiteYaourt = (TextView) this.findViewById(R.id.lblQuantiteYaourt);
        lblDureeYaourt = (TextView) this.findViewById(R.id.lblDureeYaourt);
        lblNbDegluYaourt = (TextView) this.findViewById(R.id.lblNbDegluYaourt);
        lblDureeMoyenneDegluYaourt = (TextView) this.findViewById(R.id.lblDureeMoyenneDegluYaourt);
        lblDureeMoyenneInterDegluYaourt = (TextView) this.findViewById(R.id.lblDureeMoyenneInterDegluYaourt);

        btSeeDetails = (Button) findViewById(R.id.btSeeDetails);


        lblPatient.setText("Rapport de l'examen de " + patient.getPrenom() + " " + patient.getNom() + ", " + patient.getAge() + " ans");
        lblExaminateur.setText("Examinateur " + data.get("Examinateur"));
        lblExamen.setText("Examen daté du " + examen.getDate());

        lblDureeTT.setText("Durée totale du repas : " + examen.getDurees()[0]);
        lblNbDegluTT.setText("Nombre Total de déglutitions : " + examen.getNbDeglu()[0]);
        lblDureeMoyenneDegluTT.setText("DDM Totale : " + examen.getDureesMoyenneDeglu()[0]);
        lblDureeMoyenneInterDegluTT.setText("DIDM Totale : " + examen.getDureesMoyenneInter()[0]);

        lblQuantitePuree.setText("Quantité : " + examen.getQuantitees()[0] + " ml");
        lblDureePuree.setText("Durée : " +examen.getDurees()[1]);
        lblNbDegluPuree.setText("Nombre de déglutitions : " + examen.getNbDeglu()[1]);
        lblDureeMoyenneDegluPuree.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[1]);
        lblDureeMoyenneInterDegluPuree.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[1]);

        lblQuantiteEau.setText("Quantité : " + examen.getQuantitees()[1] + " ml");
        lblDureeEau.setText("Durée : " +examen.getDurees()[2]);
        lblNbDegluEau.setText("Nombre de déglutitions : " + examen.getNbDeglu()[2]);
        lblDureeMoyenneDegluEau.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[2]);
        lblDureeMoyenneInterDegluEau.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[2]);

        lblQuantiteYaourt.setText("Quantité : " + examen.getQuantitees()[2] + " ml");
        lblDureeYaourt.setText("Durée : " +examen.getDurees()[3]);
        lblNbDegluYaourt.setText("Nombre de déglutitions : " + examen.getNbDeglu()[3]);
        lblDureeMoyenneDegluYaourt.setText("Durée moyenne des déglutitions : " + examen.getDureesMoyenneDeglu()[3]);
        lblDureeMoyenneInterDegluYaourt.setText("Durée moyenne des inter-déglutitions : " + examen.getDureesMoyenneInter()[3]);

        btSeeDetails.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ImageView leftValider = (ImageView) findViewById(R.id.leftValider);
                Button btSeeDetails = (Button) findViewById(R.id.btSeeDetails);
                ImageView rigthValider = (ImageView) findViewById(R.id.rightValider);

                leftValider.setBackground(getResources().getDrawable(R.drawable.pressedleft));
                btSeeDetails.setBackground(getResources().getDrawable(R.drawable.pressedmiddle));
                rigthValider.setBackground(getResources().getDrawable(R.drawable.pressedrigth));

                leftValider.setBackground(getResources().getDrawable(R.drawable.unpressedleft));
                btSeeDetails.setBackground(getResources().getDrawable(R.drawable.unpressedmiddle));
                rigthValider.setBackground(getResources().getDrawable(R.drawable.unpressedrigth));

                Intent intentRapport = new Intent(rapportGlobal.this, rapport.class);
                intentRapport.putExtra("Examen", data);
                startActivity(intentRapport);
            }
        });
    }

    private Patient patient;
    private Examen examen;

    private TextView lblPatient;
    private TextView lblExaminateur;
    private TextView lblExamen;

    private TextView lblDureeTT;
    private TextView lblNbDegluTT;
    private TextView lblDureeMoyenneDegluTT;
    private TextView lblDureeMoyenneInterDegluTT;

    private TextView lblQuantitePuree;
    private TextView lblDureePuree;
    private TextView lblNbDegluPuree;
    private TextView lblDureeMoyenneDegluPuree;
    private TextView lblDureeMoyenneInterDegluPuree;

    private TextView lblQuantiteEau;
    private TextView lblDureeEau;
    private TextView lblNbDegluEau;
    private TextView lblDureeMoyenneDegluEau;
    private TextView lblDureeMoyenneInterDegluEau;

    private TextView lblQuantiteYaourt ;
    private TextView lblDureeYaourt;
    private TextView lblNbDegluYaourt;
    private TextView lblDureeMoyenneDegluYaourt;
    private TextView lblDureeMoyenneInterDegluYaourt ;

    private Button btSeeDetails;
}
