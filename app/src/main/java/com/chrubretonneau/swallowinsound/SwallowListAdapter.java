package com.chrubretonneau.swallowinsound;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoDeglutition;

import java.util.HashMap;
import java.util.List;

//Cette classe permet de gérer la liste des différentes déglutition dans le détail (en dessous de la waveform)
public class SwallowListAdapter extends ArrayAdapter implements View.OnClickListener {

    private static OnDeleteClickListener mOnDeleteClickListener;

    //Mise à jour de l'affichage
    public interface OnDeleteClickListener {
        void updateDisplay();
    }

    public void setOnDeleteClickListener(OnDeleteClickListener listener){
        mOnDeleteClickListener = listener;
    }

    private DaoDeglutition bdd_tmp;
    private Context mContext;

    public SwallowListAdapter(List<HashMap<String, String>> data, Context context) {

        super(context, R.layout.layout_itemdeglutition, data);
         bdd_tmp = new DaoDeglutition(context);
        this.mContext = context;

    }

    //Récupération de l'action onlick permettant de supprimer la déglutition
    @Override
    public void onClick(final View v) {

        final int position=(Integer) v.getTag();
        final HashMap<String, String> item= (HashMap<String, String>) getItem(position);

        switch (v.getId())
        {
            case R.id.row_delete_button:
                new AlertDialog.Builder(getContext())
                        .setCancelable(true)
                        .setTitle("Suppresion")
                        .setMessage("Être vous sure de vouloir supprimer cette déglutition ?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                int id = Integer.parseInt(item.get("id"));
                                System.out.println("Déglutition id : "+ id +" Suppresion confirmée");

                                //remove inter-déglutition
                                if(position != getCount()-1){
                                    remove(getItem(position+1));
                                }

                                //APPEL DAO !!!
                                bdd_tmp.deleteDeglutitionById(id);
                                remove(item);
                                //Mise à jour de l'interface graphique
                                mOnDeleteClickListener.updateDisplay();

                                notifyDataSetChanged();
                            }
                        }).setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("Suppresion annulée");
                    }
                }).show();
                break;
        }
    }

    //Récupération de la vue et ajour de l'image clickable du bouton pour supprimer chaque déglutition
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        HashMap<String, String> dataModel = (HashMap<String, String>) getItem(position);

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_itemdeglutition, parent, false);

        }

        if(dataModel != null) {
            TextView type = convertView.findViewById(R.id.row_type);
            type.setText(dataModel.get("type"));

            TextView duree = convertView.findViewById(R.id.row_duree);
            duree.setText(dataModel.get("duree"));

            ImageView deleteButton = convertView.findViewById(R.id.row_delete_button);

            if(dataModel.get("id") == null) {
                deleteButton.setImageDrawable(null);
                deleteButton.setOnClickListener(null);

            }
            else{
                //Si l'id existe on lui assigne l'image
                deleteButton.setImageDrawable(convertView.getResources().getDrawable(R.drawable.ic_trash_alt_solid));
                deleteButton.setOnClickListener(this);
                deleteButton.setTag(position);
            }
        }
        // Return the completed view to render on screen
        return convertView;
    }

}
