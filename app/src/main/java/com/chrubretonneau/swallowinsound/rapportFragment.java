package com.chrubretonneau.swallowinsound;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoDeglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExamen;
import com.chrubretonneau.swallowinsound.ModelesClass.Deglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Examen;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;
import com.chrubretonneau.swallowinsound.WaveformView.SamplePlayer;
import com.chrubretonneau.swallowinsound.WaveformView.WaveformView;
import com.chrubretonneau.swallowinsound.WaveformView.MarkerView;
import com.chrubretonneau.swallowinsound.soundfile.SoundFile;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 * Waveform et fonction associé extraite de l'application opensource Ringdroid https://github.com/johnjohndoe/Ringdroid
 */

public class rapportFragment extends ListFragment implements MarkerView.MarkerListener, WaveformView.WaveformListener, SwallowListAdapter.OnDeleteClickListener
{

    View view;

    //region variables
    private long mLoadingLastUpdateTime;
    private boolean mLoadingKeepGoing;
    private boolean mFinishActivity;
    private AlertDialog mAlertDialog;
    private ProgressDialog mProgressDialog;
    private SoundFile mSoundFile;
    private File mFile;
    private WaveformView waveformView;
    private MarkerView mStartMarker;
    private MarkerView mEndMarker;
    private ImageButton playButton;
    private ImageButton rewindButton;
    private ImageButton ffwdButton;
    private ImageButton btnBack;
    private ImageButton btnForward;
    private ImageButton btnAddSwallow;
    private boolean mKeyDown;
    private int mWidth;
    private int mMaxPos;
    private int mStartPos;
    private int mEndPos;
    private boolean mStartVisible;
    private boolean mEndVisible;
    private int mLastDisplayedStartPos;
    private int mLastDisplayedEndPos;
    private int mOffset;
    private int mOffsetGoal;
    private int mFlingVelocity;
    private int mPlayStartMsec;
    private int mPlayEndMsec;
    private Handler mHandler;
    private boolean mIsPlaying;
    private SamplePlayer mPlayer;
    private boolean mTouchDragging;
    private float mTouchStart;
    private int mTouchInitialOffset;
    private int mTouchInitialStartPos;
    private int mTouchInitialEndPos;
    private long mWaveformTouchStartMsec;
    private float mDensity;
    private Thread mLoadSoundFileThread;
    private String aliment;
    List<Deglutition> listeDeglutitions;

    private Examen examen;
    private int idAliment = 0;


    TextView lblPatient;
    TextView lblExaminateur;
    TextView lblExamen;
    TextView lblDureeTT ;
    TextView lblNbDegluTT ;
    TextView lblDureeMoyenneDegluTT ;
    TextView lblDureeMoyenneInterDegluTT ;
    TextView lblDuree ;
    TextView lblQuantite ;
    TextView lblNbDeglu ;
    TextView lblAVGDeglu ;
    TextView lblAVGInterDeglu;

//endregion


    /**
     * Au chargement du fragment on affiche les données du patient, de l'examen et de l'aliment et on initialise et charge le waveform.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_rapport, container, false);
        Intent intent = getActivity().getIntent();

        lblPatient = (TextView) view.findViewById(R.id.lblPatient);
        lblExaminateur = (TextView) view.findViewById(R.id.lblExaminateur);
        lblExamen = (TextView) view.findViewById(R.id.lblExamin);
        lblDureeTT = (TextView) view.findViewById(R.id.lblDureeTT);
        lblNbDegluTT = (TextView) view.findViewById(R.id.lblNbDegluTT);
        lblDureeMoyenneDegluTT = (TextView) view.findViewById(R.id.lblDureeMoyenneDegluTT);
        lblDureeMoyenneInterDegluTT = (TextView) view.findViewById(R.id.lblDureeMoyenneInterDegluTT);
        lblDuree = (TextView) view.findViewById(R.id.lblDuree);
        lblQuantite = (TextView) view.findViewById(R.id.lblQuantite);
        lblNbDeglu = (TextView) view.findViewById(R.id.lblNbDeglu);
        lblAVGDeglu = (TextView) view.findViewById(R.id.lblAVGDeglu);
        lblAVGInterDeglu = (TextView) view.findViewById(R.id.lblAVGInterDeglu);

        HashMap<String, Object> data = (HashMap<String, Object>) intent.getSerializableExtra("Examen");
        Patient patient = (Patient) data.get("Patient");
        examen = (Examen) data.get("Examen");
        aliment = getArguments().getString("Aliment");
        idAliment = 1;
        switch (aliment){
            case "Puree":
                idAliment = 1;
                break;
            case "Eau":
                idAliment = 2;
                break;
            case "Yaourt":
                idAliment = 3;
                break;
        }


        mFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + patient.getPrenom() + " " + patient.getNom() + "/" + examen.getDate() + "/" + aliment + ".wav");

        lblPatient.setText("Rapport de l'examen de " + patient.getPrenom() + " " + patient.getNom() + ", " + patient.getAge() + " ans");
        lblExaminateur.setText("Examinateur " + data.get("Examinateur"));
        lblExamen.setText("Examin daté du " + examen.getDate());
        lblDureeTT.setText("Duree totale du repas : " + examen.getDurees()[0]);
        lblNbDegluTT.setText("Nombre Total de déglutitions : " + examen.getNbDeglu()[0]);
        lblDureeMoyenneDegluTT.setText("DDM Totale : " + examen.getDureesMoyenneDeglu()[0]);
        lblDureeMoyenneInterDegluTT.setText("DIDM Totale : " + examen.getDureesMoyenneInter()[0]);

        lblDuree.setText("Temps passé sur "+ aliment.toLowerCase() +" : " + examen.getDurees()[idAliment]);
        lblQuantite.setText("Quantité de " + aliment + " : " + examen.getQuantitees()[idAliment-1] + " ml");
        lblNbDeglu.setText("Nombre de déglutition : " + examen.getNbDeglu()[idAliment]);
        lblAVGDeglu.setText("DDM : " + examen.getDureesMoyenneDeglu()[idAliment]);
        lblAVGInterDeglu.setText("DIDM : " + examen.getDureesMoyenneInter()[idAliment]);

        //Chargement des DAO pour récupérer les déglutitions
        DaoDeglutition bdd_tmp = new DaoDeglutition(super.getActivity());
        listeDeglutitions = bdd_tmp.getListeDeglutition(examen.getIdExamen(), idAliment);
        bdd_tmp.close();

        chargerListeDeglu(view, listeDeglutitions);

        mPlayer = null;
        mIsPlaying = false;

        mAlertDialog = null;
        mProgressDialog = null;

        mLoadSoundFileThread = null;

        mSoundFile = null;
        mKeyDown = false;

        mHandler = new Handler();

        //region "loadGui"
        DisplayMetrics metrics = new DisplayMetrics();
        super.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mDensity = metrics.density;

        playButton = (ImageButton) view.findViewById(R.id.play);
        playButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View sender){
                onPlay(waveformView.getStart());
            }
        });
        //On récupère ici l'élement custom view pager qui correspond au container
        final CustomViewPager pager = (CustomViewPager) container;
        //On récupères le bouton pour changer de page en arrière
        btnBack = (ImageButton) view.findViewById(R.id.btnGoBack);
        btnBack.setOnClickListener(new OnClickListener()
        {
            //On permet au bouton de lui faire changer de page lorsqu'on clic dessus
            @Override
            public void onClick(View sender)
            {
                if (pager.getCurrentItem() != 0)
                    pager.setCurrentItem(pager.getCurrentItem() - 1);
                else
                    pager.setCurrentItem(pager.getAdapter().getCount());
            }
        });
        //On récupère le bouton qui permet de changer de page en avant
        btnForward = (ImageButton) view.findViewById(R.id.btnGoForward);
        btnForward.setOnClickListener(new OnClickListener()
        {
            //On permet au bouton de lui faire changer de page lorsqu'on clic dessus
            @Override
            public void onClick(View sender)
            {
                if (pager.getCurrentItem() < pager.getAdapter().getCount()-1)
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                else
                    pager.setCurrentItem(0);
            }
        });
        //On récupère le bouton pour ajouter une déglutition
        btnAddSwallow = (ImageButton) view.findViewById(R.id.btnAddSwallow);
        btnAddSwallow.setOnClickListener(new OnClickListener() {
            //On définit l'action de l'ajout de déglutition
            @Override
            public void onClick(View v) {

                NumberFormat formatter = new DecimalFormat("#0.00");

                double start = waveformView.pixelsToSeconds(waveformView.getStart()-waveformView.getOffset());
                double end = waveformView.pixelsToSeconds(waveformView.getEnd()-waveformView.getOffset());

                new AlertDialog.Builder(getContext())
                        .setCancelable(true)
                        .setTitle("Ajouter une déglutition")
                        .setMessage("Être vous sure de vouloir ajouter une déglutition entre "+formatter.format(start)+"s et "+formatter.format(end)+"s ?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                int start = waveformView.pixelsToMillisecs(waveformView.getStart()-waveformView.getOffset());
                                int end = waveformView.pixelsToMillisecs(waveformView.getEnd()-waveformView.getOffset());

                                System.out.println("#########################\nSTART : "+ start+ "\nEND : "+end+"\n#################");

                                DaoDeglutition bdd_tmp = new DaoDeglutition(getContext());
                                //APPEL DAO
                                bdd_tmp.insertDeglutition(start, end, idAliment, examen.getIdExamen());
                                bdd_tmp.close();

                                updateDisplay();
                            }
                        }).setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("Ajout annulé");
                    }
                }).show();
            }
        });
        //Récupération du bouton pour lancer la lecture au début
        rewindButton = (ImageButton) view.findViewById(R.id.rew);
        rewindButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View sender)
            {
                if (mIsPlaying)
                {
                    int newPos = mPlayer.getCurrentPosition() - 5000;
                    if (newPos < mPlayStartMsec) {
                        if (newPos - 5000 < 0) {
                            mPlayStartMsec = 0;
                            newPos = 0;
                        } else {
                            mPlayStartMsec = newPos;
                        }
                    }
                    mPlayer.seekTo(newPos);
                }
            }
        });
        //Récupération pour lancer la lecture depuis la fin
        ffwdButton = (ImageButton) view.findViewById(R.id.ffwd);
        ffwdButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View sender)
            {
                if(mIsPlaying)
                {
                    int newPos = 5000 + mPlayer.getCurrentPosition();
                    if (newPos > mPlayEndMsec)
                        newPos = mPlayEndMsec;
                    mPlayer.seekTo(newPos);
                }
            }
        });

        enableDisableButtons();

        waveformView = (WaveformView) view.findViewById(R.id.waveform);
        waveformView.setListener(this);
        waveformView.setSwallows(listeDeglutitions);

        mStartMarker = (MarkerView)view.findViewById(R.id.startmarker);
        mStartMarker.setmRLayout((ViewGroup) view.findViewById(R.id.startMarkerLayout));
        mStartMarker.setListener(this);
        mStartMarker.setAlpha(1f);
        mStartMarker.setFocusable(true);
        mStartMarker.setFocusableInTouchMode(true);
        mStartVisible = true;

        mEndMarker = (MarkerView)view.findViewById(R.id.endmarker);
        mEndMarker.setmRLayout((ViewGroup) view.findViewById(R.id.endMarkerLayout));
        mEndMarker.setListener(this);
        mEndMarker.setAlpha(1f);
        mEndMarker.setFocusable(true);
        mEndMarker.setFocusableInTouchMode(true);
        mEndVisible = true;

        mMaxPos = 0;
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        mStartVisible = true;
        mEndVisible = true;

        //Mise à jour de l'affichage
        updateDisplay();
        //endregion

        //region loadFromFile
        mLoadingLastUpdateTime = getCurrentTime();
        mLoadingKeepGoing = true;
        mFinishActivity = false;
        mProgressDialog = new ProgressDialog(super.getActivity());
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle("Chargement "+aliment.toLowerCase()+"...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            public void onCancel(DialogInterface dialog)
            {
                mLoadingKeepGoing = false;
                mFinishActivity = true;
            }
        });
        mProgressDialog.show();

        final SoundFile.ProgressListener listener = new SoundFile.ProgressListener()
        {
            public boolean reportProgress(double fractionComplete)
            {
                long now = getCurrentTime();
                if (now - mLoadingLastUpdateTime > 100)
                {
                    mProgressDialog.setProgress((int) (mProgressDialog.getMax() * fractionComplete));
                    mLoadingLastUpdateTime = now;
                }
                return mLoadingKeepGoing;
            }
        };

        // Load the sound file in a background thread
        mLoadSoundFileThread = new Thread()
        {
            public void run()
            {
                try
                {
                    mSoundFile = SoundFile.create(mFile.getAbsolutePath(), listener);

                    if (mSoundFile == null)
                    {
                        mProgressDialog.dismiss();
                        String name = mFile.getName().toLowerCase();
                        String[] components = name.split("\\.");
                        String err;
                        if (components.length < 2)
                        {
                            err = getResources().getString(R.string.no_extension_error);
                        }
                        else
                        {
                            err = getResources().getString(R.string.bad_extension_error) + " " + components[components.length - 1];
                        }
                        final String finalErr = err;
                        Runnable runnable = new Runnable()
                        {
                            public void run()
                            {
                                showFinalAlert(new Exception(), finalErr);
                            }
                        };
                        mHandler.post(runnable);
                        return;
                    }
                    mPlayer = new SamplePlayer(mSoundFile);
                }
                catch (final Exception e)
                {
                    mProgressDialog.dismiss();
                    e.printStackTrace();

                    Runnable runnable = new Runnable()
                    {
                        public void run()
                        {
                            showFinalAlert(e, getResources().getText(R.string.read_error));
                        }
                    };
                    mHandler.post(runnable);
                    return;
                }
                mProgressDialog.dismiss();
                if (mLoadingKeepGoing)
                {
                    Runnable runnable = new Runnable()
                    {
                        public void run()
                        {
                            finishOpeningSoundFile(view);
                        }
                    };
                    mHandler.post(runnable);
                }
                else if (mFinishActivity)
                {
                    rapportFragment.super.getActivity().finish();
                }
            }
        };
        mLoadSoundFileThread.start();
        //endregion

        view.setTop(0);
        return view;
    }


    private void closeThread(Thread thread) {
        if (thread != null && thread.isAlive())
        {
            try
            {
                thread.join();
            }
            catch (InterruptedException e)
            {
                System.out.println("Exeption interruption in closeThread : " + e);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible)
    {
        super.setUserVisibleHint(visible);

        if (mPlayer != null && mIsPlaying == true && (mPlayer.isPlaying() || mPlayer.isPaused()))
        {
            mPlayer.stop();
            mIsPlaying = false;
            playButton.setImageResource(android.R.drawable.ic_media_play);
            playButton.setContentDescription(getResources().getText(R.string.play));

            updateDisplay();
        }
    }


    /** Called when the activity is finally destroyed. */
    @Override
    public void onDestroy()
    {
        mLoadingKeepGoing = false;
        closeThread(mLoadSoundFileThread);
        mLoadSoundFileThread = null;
        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        if(mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        if (mPlayer != null) {
            if (mPlayer.isPlaying() || mPlayer.isPaused()) {
                mPlayer.stop();
            }
            mPlayer.release();
            mPlayer = null;
        }

        super.onDestroy();
    }

    /**
     * Every time we get a message that our waveform drew, see if we need to
     * animate and trigger another redraw.
     */
    public void waveformDraw()
    {
        mWidth = waveformView.getMeasuredWidth();
        if (mOffsetGoal != mOffset && !mKeyDown)
        {
            updateDisplay();
        }
        else if (mIsPlaying)
        {
            updateDisplay();
        }
        else if (mFlingVelocity != 0)
        {
            updateDisplay();
        }
    }

    public void waveformTouchStart(int x) {

    }

    public void waveformTouchMove(float x) {
//        mOffset = trap((int)(mTouchInitialOffset + (mTouchStart - x)));
//        updateDisplay();
    }

    @Override
    public void waveformTouchEnd() { }


    public void waveformFling(float vx) {
        mTouchDragging = false;
        mOffsetGoal = mOffset;
        mFlingVelocity = (int)(-vx);
        updateDisplay();
    }

    public void waveformZoomIn() {
        waveformView.zoomIn();
        mStartPos = waveformView.getStart();
        mEndPos = waveformView.getEnd();
        mMaxPos = waveformView.maxPos();
        mOffset = waveformView.getOffset();
        mOffsetGoal = mOffset;
        updateDisplay();
    }

    public void waveformZoomOut() {
        waveformView.zoomOut();
        mStartPos = waveformView.getStart();
        mEndPos = waveformView.getEnd();
        mMaxPos = waveformView.maxPos();
        mOffset = waveformView.getOffset();
        mOffsetGoal = mOffset;
        updateDisplay();
    }

    //
    // MarkerListener
    //

    public void markerDraw() {
    }

    public void markerTouchStart(MarkerView marker, float x) {
        mTouchDragging = true;
        mTouchStart = x;
        mTouchInitialStartPos = mStartPos;
        mTouchInitialEndPos = mEndPos;
    }

    public void markerTouchMove(MarkerView marker, float x) {
        float delta = x - mTouchStart;

        if (marker == mStartMarker) {
            mStartPos = trap((int)(mTouchInitialStartPos + delta));
            mEndPos = trap((int)(mTouchInitialEndPos + delta));
        } else {
            mEndPos = trap((int)(mTouchInitialEndPos + delta));
            if (mEndPos < mStartPos)
                mEndPos = mStartPos;
        }

        updateDisplay();
    }

    public void markerTouchEnd(MarkerView marker) {
        mTouchDragging = false;
        if (marker == mStartMarker) {
            setOffsetGoalStart();
        } else {
            setOffsetGoalEnd();
        }
    }

    public void markerLeft(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos = trap(mStartPos - velocity);
            mEndPos = trap(mEndPos - (saveStart - mStartPos));
            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            if (mEndPos == mStartPos) {
                mStartPos = trap(mStartPos - velocity);
                mEndPos = mStartPos;
            } else {
                mEndPos = trap(mEndPos - velocity);
            }

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    public void markerRight(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos += velocity;
            if (mStartPos > mMaxPos)
                mStartPos = mMaxPos;
            mEndPos += (mStartPos - saveStart);
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            mEndPos += velocity;
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    public void markerEnter(MarkerView marker) {
    }

    public void markerKeyUp() {
        mKeyDown = false;
        updateDisplay();
    }

    public void markerFocus(MarkerView marker) {
        mKeyDown = false;
        if (marker == mStartMarker) {
            setOffsetGoalStartNoUpdate();
        } else {
            setOffsetGoalEndNoUpdate();
        }

        // Delay updaing the display because if this focus was in
        // response to a touch event, we want to receive the touch
        // event too before updating the display.
        mHandler.postDelayed(new Runnable() {
            public void run() {
                updateDisplay();
            }
        }, 100);
    }

    //END MARKER LISTENER


    private void finishOpeningSoundFile(View view) {
        waveformView.setSoundFile(mSoundFile);
        waveformView.recomputeHeights(mDensity);

        mMaxPos = waveformView.maxPos();
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        mTouchDragging = false;

        mOffset = 0;
        mOffsetGoal = 0;
        mFlingVelocity = 0;
        resetPositions();
        if (mEndPos > mMaxPos)
            mEndPos = mMaxPos;



        updateDisplay();
    }



    public synchronized void updateDisplay() {

        DaoExamen daoExamen = new DaoExamen(getContext());
        examen = daoExamen.getExamen(examen.getIdExamen());


        lblDureeTT.setText("Duree totale du repas : " + examen.getDurees()[0]);
        lblNbDegluTT.setText("Nombre Total de déglutitions : " + examen.getNbDeglu()[0]);
        lblDureeMoyenneDegluTT.setText("DDM Totale : " + examen.getDureesMoyenneDeglu()[0]);
        lblDureeMoyenneInterDegluTT.setText("DIDM Totale : " + examen.getDureesMoyenneInter()[0]);

        lblDuree.setText("Temps passé sur "+ aliment.toLowerCase() +" : " + examen.getDurees()[idAliment]);
        lblQuantite.setText("Quantité de " + aliment + " : " + examen.getQuantitees()[idAliment-1] + " ml");
        lblNbDeglu.setText("Nombre de déglutition : " + examen.getNbDeglu()[idAliment]);
        lblAVGDeglu.setText("DDM : " + examen.getDureesMoyenneDeglu()[idAliment]);
        lblAVGInterDeglu.setText("DIDM : " + examen.getDureesMoyenneInter()[idAliment]);

        DaoDeglutition bdd_tmp = new DaoDeglutition(super.getActivity());
        listeDeglutitions = bdd_tmp.getListeDeglutition(examen.getIdExamen(), idAliment);
        bdd_tmp.close();
        chargerListeDeglu(view, listeDeglutitions);
        waveformView.setSwallows(listeDeglutitions);

        if (mIsPlaying) {
            int now = mPlayer.getCurrentPosition();
            int frames = waveformView.millisecsToPixels(now);
            waveformView.setPlayback(frames);
            setOffsetGoalNoUpdate(frames - mWidth / 2);
            if (now >= mPlayEndMsec) {
                handlePause();
            }
        }


        if (!mTouchDragging) {
            int offsetDelta;

            if (mFlingVelocity != 0) {
                offsetDelta = mFlingVelocity / 30;
                if (mFlingVelocity > 80) {
                    mFlingVelocity -= 80;
                } else if (mFlingVelocity < -80) {
                    mFlingVelocity += 80;
                } else {
                    mFlingVelocity = 0;
                }

                mOffset += offsetDelta;

                if (mOffset + mWidth / 2 > mMaxPos) {
                    mOffset = mMaxPos - mWidth / 2;
                    mFlingVelocity = 0;
                }
                if (mOffset < 0) {
                    mOffset = 0;
                    mFlingVelocity = 0;
                }
                mOffsetGoal = mOffset;
            } else {
                offsetDelta = mOffsetGoal - mOffset;

                if (offsetDelta > 10)
                    offsetDelta = offsetDelta / 10;
                else if (offsetDelta > 0)
                    offsetDelta = 1;
                else if (offsetDelta < -10)
                    offsetDelta = offsetDelta / 10;
                else if (offsetDelta < 0)
                    offsetDelta = -1;
                else
                    offsetDelta = 0;

                mOffset += offsetDelta;
            }
        }

        mStartMarker.setPosition(mStartPos, mOffset);
        mStartMarker.invalidate();

        mEndMarker.setPosition(mEndPos, mOffset);
        mEndMarker.invalidate();

        waveformView.setParameters(mStartPos, mEndPos, mOffset);
        waveformView.invalidate();

        int startX = mStartPos - mOffset;
        if (startX >= 0) {
            if (!mStartVisible) {
                // Delay this to avoid flicker
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        mStartVisible = true;
                    }
                }, 0);
            }
        } else {
            if (mStartVisible) {
                mStartVisible = false;
            }
            startX = 0;
        }

        int endX = mEndPos - mOffset;
        if (endX >= 0) {
            if (!mEndVisible) {
                // Delay this to avoid flicker
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        mEndVisible = true;
                    }
                }, 0);
            }
        } else {
            if (mEndVisible) {
                mEndVisible = false;
            }
            endX = 0;
        }
    }


    private void enableDisableButtons() {
        if (mIsPlaying) {
            playButton.setImageResource(android.R.drawable.ic_media_pause);
            playButton.setContentDescription(getResources().getText(R.string.stop));
        } else {
            playButton.setImageResource(android.R.drawable.ic_media_play);
            playButton.setContentDescription(getResources().getText(R.string.play));
        }
    }

    private void resetPositions() {
        mStartPos = waveformView.secondsToPixels(0.0);
        mEndPos = waveformView.secondsToPixels(mMaxPos);
    }

    private int trap(int pos) {
        if (pos < 0)
            return 0;
        if (pos > mMaxPos)
            return mMaxPos;
        return pos;
    }

    private void setOffsetGoalStart() {
        setOffsetGoal(mStartPos - mWidth / 2);
    }

    private void setOffsetGoalStartNoUpdate() {
        setOffsetGoalNoUpdate(mStartPos - mWidth / 2);
    }

    private void setOffsetGoalEnd() {
        setOffsetGoal(mEndPos - mWidth / 2);
    }

    private void setOffsetGoalEndNoUpdate() {
        setOffsetGoalNoUpdate(mEndPos - mWidth / 2);
    }

    private void setOffsetGoal(int offset) {
        setOffsetGoalNoUpdate(offset);
        updateDisplay();
    }

    private void setOffsetGoalNoUpdate(int offset) {
        if (mTouchDragging) {
            return;
        }

        mOffsetGoal = offset;
        if (mOffsetGoal + mWidth / 2 > mMaxPos)
            mOffsetGoal = mMaxPos - mWidth / 2;
        if (mOffsetGoal < 0)
            mOffsetGoal = 0;
    }

    private String formatTime(int pixels) {
        if (waveformView != null && waveformView.isInitialized()) {
            return formatDecimal(waveformView.pixelsToSeconds(pixels));
        } else {
            return "";
        }
    }

    private String formatDecimal(double x) {
        int xWhole = (int)x;
        int xFrac = (int)(100 * (x - xWhole) + 0.5);

        if (xFrac >= 100) {
            xWhole++; //Round up
            xFrac -= 100; //Now we need the remainder after the round up
            if (xFrac < 10) {
                xFrac *= 10; //we need a fraction that is 2 digits long
            }
        }

        if (xFrac < 10)
            return xWhole + ".0" + xFrac;
        else
            return xWhole + "." + xFrac;
    }

    private synchronized void handlePause() {
        if (mPlayer != null && mPlayer.isPlaying())
        {
            mPlayer.pause();
            mPlayStartMsec = mPlayer.getCurrentPosition();
            mPlayer.seekTo(mPlayStartMsec);
        }
        waveformView.setPlayback(-1);
        mIsPlaying = false;
        enableDisableButtons();
    }

    private synchronized void onPlay(int startPosition) {
        if (mIsPlaying) {
            handlePause();
            return;
        }

        if (mPlayer == null) {
            // Not initialized yet
            return;
        }

        if(mStartPos == 0 && mPlayEndMsec != 0)
        {
            mPlayEndMsec = waveformView.pixelsToMillisecs(mStartPos);
        }

        try {
            mPlayStartMsec = waveformView.pixelsToMillisecs(startPosition);
            mPlayEndMsec = waveformView.pixelsToMillisecs(mMaxPos);

            mPlayer.setOnCompletionListener(new SamplePlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion()
                {
                    handlePause();
                }
            });
            mIsPlaying = true;

            mPlayer.seekTo(mPlayStartMsec);
            mPlayer.start();
            updateDisplay();
            enableDisableButtons();
        } catch (Exception e) {
            showFinalAlert(e, R.string.play_error);
        }
    }

    /**
     * Show a "final" alert dialog that will exit the activity
     * after the user clicks on the OK button.  If an exception
     * is passed, it's assumed to be an error condition, and the
     * dialog is presented as an error, and the stack trace is
     * logged.  If there's no exception, it's a success message.
     */
    private void showFinalAlert(Exception e, CharSequence message) {
        CharSequence title;
        if (e != null) {
            Log.e("Ringdroid", "Error: " + message);
            Log.e("Ringdroid", getStackTrace(e));
            title = getResources().getText(R.string.alert_title_failure);
            super.getActivity().setResult(super.getActivity().RESULT_CANCELED, new Intent());
        } else {
            Log.v("Ringdroid", "Success: " + message);
            title = getResources().getText(R.string.alert_title_success);
        }

        new AlertDialog.Builder(super.getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(
                        R.string.alert_ok_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                rapportFragment.super.getActivity().finish();
                            }
                        })
                .setCancelable(false)
                .show();
    }

    private void showFinalAlert(Exception e, int messageResourceId) {
        showFinalAlert(e, getResources().getText(messageResourceId));
    }



    private long getCurrentTime() {
        return System.nanoTime() / 1000000;
    }

    private String getStackTrace(Exception e) {
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }


    /**
     * La fonction chargerListeDeglu permet de mettre en forme et de charger la liste des durées des déglutitions et inter-déglutitions.
     * @param view
     * @param listeDeglutitions
     */
    public void chargerListeDeglu(View view, final List<Deglutition> listeDeglutitions)
    {
        if(listeDeglutitions.size() != 0)
        {
            SwallowListAdapter adapter;
            final List<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> temp  = new HashMap<String, String>();

            Deglutition uneDeglu = listeDeglutitions.get(0);

            temp.put("type", "Déglutition1");
            temp.put("duree", (uneDeglu.getTimeEndDeglutition() - uneDeglu.getTimeBeginDeglutition()) + " ms");
            temp.put("id", String.valueOf(uneDeglu.getIdDeglutition()));

            dataList.add(temp);

            for (int i = 1; i < listeDeglutitions.size(); i++) {
                uneDeglu = listeDeglutitions.get(i);
                int inter = uneDeglu.getTimeBeginDeglutition() - listeDeglutitions.get(i - 1).getTimeEndDeglutition();

                temp = new HashMap<String, String>();
                temp.put("type", "inter-deglution" + i + "." + (i + 1));
                temp.put("duree", inter + " ms");
                temp.put("id", null);

                dataList.add(temp);

                temp = new HashMap<String, String>();
                temp.put("type", "Déglutition" + (i + 1));
                temp.put("duree", (uneDeglu.getTimeEndDeglutition() - uneDeglu.getTimeBeginDeglutition()) + " ms");
                temp.put("id", String.valueOf(uneDeglu.getIdDeglutition()));

                dataList.add(temp);
            }

            //Récupération du composant ListView
            ListView lvDeglus = (ListView) view.findViewById(android.R.id.list);

            //Création du SimpleAdapter qui se chargera de mettre les items présent dans la listView de la page home
            //Un item de dataList = un Hashmap avec comme clé : (type, duree, id)
            adapter = new SwallowListAdapter(dataList,this.getContext());

            //Initialisation de la liste avec les données placées dans le SimpleAdapter
            lvDeglus.setAdapter(adapter);

            adapter.setOnDeleteClickListener(this);

        }
    }
}