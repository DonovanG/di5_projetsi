package com.chrubretonneau.swallowinsound;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExamen;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExport;
import com.chrubretonneau.swallowinsound.ModelesClass.Permission;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;

/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class home extends Activity {

    private ListView listExamens;
    private TextView txt_no_data;
    private boolean longClickListview = false;
    public static SimpleAdapter adapter;
    public static ArrayList<HashMap<String, String>> examens = new ArrayList<HashMap<String, String>>();

    /**
     * Au chargement de l'activité on charge l'historiqie des examens. <br/>
     * On met également au place la popup Calendar pour entrer la date de naissance.
     *
     * @param icicle
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_home);
        this.setTitle(getResources().getString(R.string.short_app_name));

        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            showFinalAlert(getResources().getText(R.string.sdcard_readonly));
            return;
        }
        if (status.equals(Environment.MEDIA_SHARED)) {
            showFinalAlert(getResources().getText(R.string.sdcard_shared));
            return;
        }
        if (!status.equals(Environment.MEDIA_MOUNTED)) {
            showFinalAlert(getResources().getText(R.string.no_sdcard));
            return;
        }

        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //Récupération de   la liste des personnes
        DaoExamen bdd = new DaoExamen(this);

        examens = bdd.getExamenList();
        bdd.close();

        sortExamens();

        txt_no_data = findViewById(R.id.txt_no_data);
        if (examens.isEmpty()) {
            txt_no_data.setText("No record to display");
            txt_no_data.setVisibility(TextView.VISIBLE);
        } else {
            txt_no_data.setText("");
            txt_no_data.setVisibility(TextView.INVISIBLE);
        }

        try {
            //Récupération du composant ListView
            listExamens = (ListView) findViewById(R.id.homeList);


            //Création du SimpleAdapter qui se chargera de mettre les items présent dans la listView de la page home
            adapter = new SimpleAdapter(this.getBaseContext(), examens, R.layout.layout_itemexamen,
                    new String[]{"idExamen", "Patient", "Date", "Examinateur", "Duree"},
                    new int[]{R.id.row_idExamen, R.id.row_patient, R.id.row_date,
                            R.id.row_examinateur, R.id.row_duree});

            //Initialisation de la liste avec les données placées dans le SimpleAdapter
            listExamens.setAdapter(adapter);

            //Ajout d'un listener sur la listView
            listExamens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if (longClickListview == false) {
                        //récupération des données de la cellule cliquée
                        HashMap<String, String> selected = (HashMap<String, String>) listExamens.getItemAtPosition(position);

                        Intent intentRapport = new Intent(home.this, rapportGlobal.class);
                        intentRapport.putExtra("Examen", selected);
                        startActivity(intentRapport);
                    }
                }
            });

            //Ajout d'un listener sur la listView
            listExamens.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    longClickListview = true;

                    new AlertDialog.Builder(home.this)
                            .setTitle("Suppression")
                            .setMessage("Voulez-vous supprimer cet examen?")
                            .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    HashMap<String, String> selected = (HashMap<String, String>) listExamens.getItemAtPosition(position);

                                    //Suppression de l'enregistrement de la base de données
                                    DaoExamen bdd = new DaoExamen(home.this);
                                    bdd.deleteExamen(Integer.parseInt(selected.get("idExamen")));

                                    //Suppression des fichiers de la mémoire
                                    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + selected.get("Patient") + "/" + selected.get("Date") + "/";
                                    File examenFolder = new File(path);
                                    for (final File alimentRecord : examenFolder.listFiles()) {
                                        alimentRecord.delete();
                                    }
                                    //Suppression du dossier de l'examen
                                    examenFolder.delete();

                                    //On se place sur le dossier parent, dossier du patient contenant tout ses examens
                                    path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + selected.get("Patient") + "/";
                                    examenFolder = new File(path);
                                    //Si le dossier est vide on le supprime
                                    if(examenFolder.listFiles().length == 0)
                                    {
                                        examenFolder.delete();
                                    }

                                    //Mise à jour de la liste des examens de la page d'accueil
                                    examens.remove(selected);
                                    adapter.notifyDataSetChanged();

                                    //On libère le clic
                                    longClickListview = false;
                                }
                            })
                            .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    longClickListview = false;
                                }
                            })
                            .show();

                    return true;
                }
            });

            SearchView mFilter = findViewById(R.id.list_search);
            mFilter.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return true;
                }
            });

        } catch (Exception e) {
            System.out.println("Erreur : " + e);
        }
    }


    private void showFinalAlert(CharSequence message) {
        new AlertDialog.Builder(home.this)
                .setTitle(getResources().getText(R.string.alert_title_failure))
                .setMessage(message)
                .setPositiveButton(
                        R.string.alert_ok_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                finish();
                            }
                        })
                .setCancelable(false)
                .show();
    }

    private void Export() {
        DaoExport temp = new DaoExport(this);
        if (temp.export()) {
            Toast.makeText(this, "Export Terminé", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Echec de l'exportation!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sortExamens() {
        Collections.sort(examens, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                return Objects.requireNonNull( o1.get("Patient")).toUpperCase()
                        .compareTo(Objects.requireNonNull(o2.get("Patient")).toUpperCase());
            }
        });
    }



    //region ActionBar
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.select_options, menu);
//;
//        SearchView mFilter = (SearchView) menu.findItem(R.id.action_search_filter).getActionView();
//        mFilter.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//                @Override
//                public boolean onQueryTextChange(String newText) {
//                    adapter.getFilter().filter(newText);
//                    return true;
//                }
//                @Override
//                public boolean onQueryTextSubmit(String query) {
//                    return true;
//                }
//            });
//
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        super.onPrepareOptionsMenu(menu);
//        menu.findItem(R.id.action_record).setVisible(true);
//        menu.findItem(R.id.action_newpatient).setVisible(true);
//        menu.findItem(R.id.action_exporter).setVisible(true);
//        menu.findItem(R.id.action_tester).setVisible(true);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_record:
//                Intent intentRepas = new Intent(home.this, repas.class);
//                home.this.startActivity(intentRepas);
//
//                return true;
//            case R.id.action_newpatient:
//                Intent intentNewPatient = new Intent(home.this, newpatient.class);
//                home.this.startActivity(intentNewPatient);
//                return true;
//            case R.id.action_tester:
//                Intent intentTest = new Intent(home.this, Test.class);
//                home.this.startActivity(intentTest);
//                return true;
//
//            case R.id.action_exporter:
//                new AlertDialog.Builder(this)
//                .setTitle("Exportation")
//                .setMessage("Exporter la base de données?")
//                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface arg0, int arg1) {
//                        Export();
//                    }
//                })
//                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface arg0, int arg1) {
//                        // Some stuff to do when cancel got clicked
//                    }
//                })
//                .show();
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
    //endregion
}
