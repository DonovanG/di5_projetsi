package com.chrubretonneau.swallowinsound;

import java.util.Vector;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
/**
 * Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class rapport extends FragmentActivity {

    CustomViewPager pager;
    private PagerAdapter rapportPagerAdapter;
    int previousState, currentState;
    /**
     * A la création, on créé la liste des rapportFragment en leur communiquant l'aliment à afficher. <br/>
     * On implémente également le boucle sur le swipe des fragments.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_rapport);

        // Création de la liste de Fragments que fera défiler le PagerAdapter
        Vector fragments = new Vector();
        Fragment puree = Fragment.instantiate(this,rapportFragment.class.getName());
        Fragment eau = Fragment.instantiate(this,rapportFragment.class.getName());
        Fragment yaourt = Fragment.instantiate(this,rapportFragment.class.getName());

        // Ajout des Fragments dans la liste
        Bundle myBundle = new Bundle();
        myBundle.putString("Aliment", "Puree");
        puree.setArguments(myBundle);
        fragments.add(puree);

        myBundle = new Bundle();
        myBundle.putString("Aliment", "Eau");
        eau.setArguments(myBundle);
        fragments.add(eau);

        myBundle = new Bundle();
        myBundle.putString("Aliment", "Yaourt");
        yaourt.setArguments(myBundle);
        fragments.add(yaourt);


        // Création de l'adapter qui s'occupera de l'affichage de la liste de Fragments
        this.rapportPagerAdapter = new rapportAdapterSwipe(super.getSupportFragmentManager(), fragments);

        pager = (CustomViewPager) super.findViewById(R.id.viewpager);
        pager.disableScroll(true);
        // Affectation de l'adapter au ViewPager
        pager.setAdapter(this.rapportPagerAdapter);
        pager.setOffscreenPageLimit(2);

        pager.addOnPageChangeListener(new CustomViewPager.OnPageChangeListener(){
            @Override
            public void onPageSelected(int pageSelected){}

            @Override
            public void onPageScrolled(int pageSected, float positionOffset, int positionOffsetPixel){}

            @Override
            public void onPageScrollStateChanged(int state) {
//                int currentPage = pager.getCurrentItem();
//                if (currentPage == 2 || currentPage == 0) {
//                    previousState = currentState;
//                    currentState = state;
//                    if (previousState == 1 && currentState == 0) {
//                        pager.setCurrentItem(currentPage == 0 ? 2 : 0);
//                    }
//                }
                pager.setCurrentItem(pager.getCurrentItem());
            }
        });


    }
}