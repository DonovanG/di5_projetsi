package com.chrubretonneau.swallowinsound.soundfile;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import java.util.List;
import java.util.Vector;

import com.chrubretonneau.swallowinsound.R;
import com.chrubretonneau.swallowinsound.Utilities.Filter;

//from: http://www.labbookpages.co.uk/audio/javaWavFiles.html
//added short and readchannel

public class Wav {
    // Wav file IO class
    // A.Greensted
    // http://www.labbookpages.co.uk

    // File format is based on the information from
    // http://www.sonicspot.com/guide/wavefiles.html
    // http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/wave.htm

    // Version 1.0
    public static final int rate = 44100;// = RECORDER_SAMPLERATE ajouté pour permettre une constante en entrée et sortie

    public static class WavData
    {
        private enum IOState {READING, WRITING, CLOSED};
        private final static int BUFFER_SIZE = 4096;

        private final static int FMT_CHUNK_ID = 0x20746D66;
        private final static int DATA_CHUNK_ID = 0x61746164;
        private final static int RIFF_CHUNK_ID = 0x46464952;
        private final static int RIFF_TYPE_ID = 0x45564157;

        private File file;                      // File that will be read from or written to
        private IOState ioState;                // Specifies the IO State of the Wav File (used for snaity checking)
        private int bytesPerSample;         // Number of bytes required to store a single sample
        private long numFrames;                 // Number of frames within the data section
        private FileOutputStream oStream;   // Output stream used for writting data
        private FileInputStream iStream;        // Input stream used for reading data
        private double floatScale;              // Scaling factor used for int <-> float conversion
        private double floatOffset;         // Offset factor used for int <-> float conversion
        private boolean wordAlignAdjust;        // Specify if an extra byte at the end of the data chunk is required for word alignment

        // Wav Header
        private int numChannels;                // 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535)
        private long sampleRate;                // 4 bytes unsigned, 0x00000001 (1) to 0xFFFFFFFF (4,294,967,295)
        // Although a java int is 4 bytes, it is signed, so need to use a long
        private int blockAlign;                 // 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535)
        private int validBits;                  // 2 bytes unsigned, 0x0002 (2) to 0xFFFF (65,535)

        // Buffering
        private byte[] buffer;                  // Local buffer used for IO
        private int bufferPointer;              // Points to the current position in local buffer
        private int bytesRead;                  // Bytes read after last read into local buffer
        private long frameCounter;              // Current number of frames read or written

        // Cannot instantiate WavFile directly, must either use newWavFile() or openWavFile()
        private WavData()
        {
            buffer = new byte[BUFFER_SIZE];
        }

        public int getNumChannels()
        {
            return numChannels;
        }


        public static WavData openWavFile(File file) throws IOException, WavFileException
        {
            // Instantiate new Wavfile and store the file reference
            WavData wavData = new WavData();
            wavData.file = file;

            // Create a new file input stream for reading file data
            wavData.iStream = new FileInputStream(file);

            // Read the first 12 bytes of the file
            int bytesRead = wavData.iStream.read(wavData.buffer, 0, 12);
            if (bytesRead != 12) throw new WavFileException("Not enough wav file bytes for header");

            // Extract parts from the header
            long riffChunkID = getLE(wavData.buffer, 0, 4);
            long chunkSize = getLE(wavData.buffer, 4, 4);
            long riffTypeID = getLE(wavData.buffer, 8, 4);

            // Check the header bytes contains the correct signature
            if (riffChunkID != RIFF_CHUNK_ID) throw new WavFileException("Invalid Wav Header data, incorrect riff chunk ID");
            if (riffTypeID != RIFF_TYPE_ID) throw new WavFileException("Invalid Wav Header data, incorrect riff type ID");

            // Check that the file size matches the number of bytes listed in header
            if (file.length() != chunkSize+8) {
                throw new WavFileException("Header chunk size (" + chunkSize + ") does not match file size (" + file.length() + ")");
            }

            boolean foundFormat = false;
            boolean foundData = false;

            // Search for the Format and Data Chunks
            while (true)
            {
                // Read the first 8 bytes of the chunk (ID and chunk size)
                bytesRead = wavData.iStream.read(wavData.buffer, 0, 8);
                if (bytesRead == -1) throw new WavFileException("Reached end of file without finding format chunk");
                if (bytesRead != 8) throw new WavFileException("Could not read chunk header");

                // Extract the chunk ID and Size
                long chunkID = getLE(wavData.buffer, 0, 4);
                chunkSize = getLE(wavData.buffer, 4, 4);

                // Word align the chunk size
                // chunkSize specifies the number of bytes holding data. However,
                // the data should be word aligned (2 bytes) so we need to calculate
                // the actual number of bytes in the chunk
                long numChunkBytes = (chunkSize%2 == 1) ? chunkSize+1 : chunkSize;

                if (chunkID == FMT_CHUNK_ID)
                {
                    // Flag that the format chunk has been found
                    foundFormat = true;

                    // Read in the header info
                    bytesRead = wavData.iStream.read(wavData.buffer, 0, 16);

                    // Check this is uncompressed data
                    int compressionCode = (int) getLE(wavData.buffer, 0, 2);
                    if (compressionCode != 1) throw new WavFileException("Compression Code " + compressionCode + " not supported");

                    // Extract the format information
                    wavData.numChannels = (int) getLE(wavData.buffer, 2, 2);
                    wavData.sampleRate = getLE(wavData.buffer, 4, 4);
                    wavData.blockAlign = (int) getLE(wavData.buffer, 12, 2);
                    wavData.validBits = (int) getLE(wavData.buffer, 14, 2);

                    if (wavData.numChannels == 0) throw new WavFileException("Number of channels specified in header is equal to zero");
                    if (wavData.blockAlign == 0) throw new WavFileException("Block Align specified in header is equal to zero");
                    if (wavData.validBits < 2) throw new WavFileException("Valid Bits specified in header is less than 2");
                    if (wavData.validBits > 64) throw new WavFileException("Valid Bits specified in header is greater than 64, this is greater than a long can hold");

                    // Calculate the number of bytes required to hold 1 sample
                    wavData.bytesPerSample = (wavData.validBits + 7) / 8;
                    if (wavData.bytesPerSample * wavData.numChannels != wavData.blockAlign)
                        throw new WavFileException("Block Align does not agree with bytes required for validBits and number of channels");

                    // Account for number of format bytes and then skip over
                    // any extra format bytes
                    numChunkBytes -= 16;
                    if (numChunkBytes > 0) wavData.iStream.skip(numChunkBytes);
                }
                else if (chunkID == DATA_CHUNK_ID)
                {
                    // Check if we've found the format chunk,
                    // If not, throw an exception as we need the format information
                    // before we can read the data chunk
                    if (foundFormat == false) throw new WavFileException("Data chunk found before Format chunk");

                    // Check that the chunkSize (wav data length) is a multiple of the
                    // block align (bytes per frame)
                    if (chunkSize % wavData.blockAlign != 0) throw new WavFileException("Data Chunk size is not multiple of Block Align");

                    // Calculate the number of frames
                    wavData.numFrames = chunkSize / wavData.blockAlign;

                    // Flag that we've found the wave data chunk
                    foundData = true;

                    break;
                }
                else
                {
                    // If an unknown chunk ID is found, just skip over the chunk data
                    wavData.iStream.skip(numChunkBytes);
                }
            }

            // Throw an exception if no data chunk has been found
            if (foundData == false) throw new WavFileException("Did not find a data chunk");

            // Calculate the scaling factor for converting to a normalised double
            if (wavData.validBits > 8)
            {
                // If more than 8 validBits, data is signed
                // Conversion required dividing by magnitude of max negative value
                wavData.floatOffset = 0;
                wavData.floatScale = 1 << (wavData.validBits - 1);
            }
            else
            {
                // Else if 8 or less validBits, data is unsigned
                // Conversion required dividing by max positive value
                wavData.floatOffset = -1;
                wavData.floatScale = 0.5 * ((1 << wavData.validBits) - 1);
            }

            wavData.bufferPointer = 0;
            wavData.bytesRead = 0;
            wavData.frameCounter = 0;
            wavData.ioState = IOState.READING;

            return wavData;
        }

        // Get and Put little endian data from local buffer
        // ------------------------------------------------
        private static long getLE(byte[] buffer, int pos, int numBytes)
        {
            numBytes --;
            pos += numBytes;

            long val = buffer[pos] & 0xFF;
            for (int b=0 ; b<numBytes ; b++) val = (val << 8) + (buffer[--pos] & 0xFF);

            return val;
        }


        private long readSample() throws IOException, WavFileException
        {
            long val = 0;

            for (int b=0 ; b<bytesPerSample ; b++)
            {
                if (bufferPointer == bytesRead)
                {
                    int read = iStream.read(buffer, 0, BUFFER_SIZE);
                    if (read == -1) throw new WavFileException("Not enough data available");
                    bytesRead = read;
                    bufferPointer = 0;
                }

                int v = buffer[bufferPointer];
                if (b < bytesPerSample-1 || bytesPerSample == 1) v &= 0xFF;
                val += v << (b * 8);

                bufferPointer ++;
            }

            return val;
        }

        public int readFrames(double[] sampleBuffer, int numFramesToRead) throws IOException, WavFileException
        {
            return readFrames(sampleBuffer, 0, numFramesToRead);
        }

        public int readFrames(double[] sampleBuffer, int offset, int numFramesToRead) throws IOException, WavFileException
        {
            if (ioState != IOState.READING) throw new IOException("Cannot read from WavFile instance");

            for (int f=0 ; f<numFramesToRead ; f++)
            {
                if (frameCounter == numFrames) return f;

                for (int c=0 ; c<numChannels ; c++)
                {
                    sampleBuffer[offset] = floatOffset + (double) readSample() / floatScale;
                    offset ++;
                }

                frameCounter ++;
            }

            return numFramesToRead;
        }

        public void close() throws IOException
        {
            // Close the input stream and set to null
            if (iStream != null)
            {
                iStream.close();
                iStream = null;
            }

            if (oStream != null)
            {
                // Write out anything still in the local buffer
                if (bufferPointer > 0) oStream.write(buffer, 0, bufferPointer);

                // If an extra byte is required for word alignment, add it to the end
                if (wordAlignAdjust) oStream.write(0);

                // Close the stream and set to null
                oStream.close();
                oStream = null;
            }

            // Flag that the stream is closed
            ioState = IOState.CLOSED;
        }

        public void display()
        {
            display(System.out);
        }

        public void display(PrintStream out)
        {
            out.printf("File: %s\n", file);
            out.printf("Channels: %d, Frames: %d\n", numChannels, numFrames);
            out.printf("IO State: %s\n", ioState);
            out.printf("Sample Rate: %d, Block Align: %d\n", sampleRate, blockAlign);
            out.printf("Valid Bits: %d, Bytes per sample: %d\n", validBits, bytesPerSample);
        }
    }

    public static class WavFileException extends Exception
    {
        private static final long serialVersionUID = 8236151366330602556L;

        public WavFileException(String message)
        {
            super(message);
        }
    }





/*--------  Functions find in http://www.edumobile.org/android/audio-recording-in-wav-format-in-android-programming/  --------*/

    public static class Record
    {
        private static final int RECORDER_BPP = 16;
        private static final int RECORDER_SAMPLERATE = rate;
        private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
        private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

        private AudioRecord recorder = null;
        private int bufferSize = 0;
        private Thread recordingThread = null;
        private boolean isRecording = false;
        String tempFilename, filename;

        public void init(String path, String aliment)
        {
            tempFilename = path + "/temp" + aliment + ".raw";
            filename = path + "/" + aliment + ".wav";

            bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
        }


        public void startRecording() {
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);

            int i = recorder.getState();
            if(i==1)
                recorder.startRecording();

            isRecording = true;

            recordingThread = new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    writeAudioDataToFile();
                }
            },"AudioRecorder Thread");

            recordingThread.start();
        }

        private void writeAudioDataToFile(){
            byte data[] = new byte[bufferSize];
            String filename = tempFilename;
            FileOutputStream os = null;

            try
            {
                os = new FileOutputStream(filename);
            }
            catch (FileNotFoundException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int read = 0;

            if(null != os)
            {
                while(isRecording)
                {
                    read = recorder.read(data, 0, bufferSize);

                    if(AudioRecord.ERROR_INVALID_OPERATION != read)
                    {
                        try
                        {
                            os.write(data);
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

                try
                {
                    os.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        public void stopRecording()
        {
            if(null != recorder)
            {
                isRecording = false;

                int i = recorder.getState();
                if(i==1)
                {
                    recorder.stop();
                }
                recorder.release();

                recorder = null;
                recordingThread = null;
            }

            copyWaveFile(tempFilename, filename);
            deleteTempFile();
        }

        private void deleteTempFile()
        {
            File file = new File(tempFilename);

            file.delete();
        }

        private void copyWaveFile(String inFilename, String outFilename)
        {
            FileInputStream in = null;
            FileOutputStream out = null;
            long totalAudioLen = 0;
            long totalDataLen = totalAudioLen + 36;
            long longSampleRate = RECORDER_SAMPLERATE;
            int channels = 2;
            long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;

            byte[] data = new byte[bufferSize];

            try
            {
                in = new FileInputStream(inFilename);
                out = new FileOutputStream(outFilename);
                totalAudioLen = in.getChannel().size();
                totalDataLen = totalAudioLen + 36;

                System.out.println("File size: " + totalDataLen);

                WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);

                while(in.read(data) != -1)
                {
                    out.write(data);
                }

                in.close();
                out.close();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen, long totalDataLen, long longSampleRate, int channels, long byteRate) throws IOException
        {
            byte[] header = new byte[44];

            header[0] = 'R'; // RIFF/WAVE header
            header[1] = 'I';
            header[2] = 'F';
            header[3] = 'F';
            header[4] = (byte) (totalDataLen & 0xff);
            header[5] = (byte) ((totalDataLen >> 8) & 0xff);
            header[6] = (byte) ((totalDataLen >> 16) & 0xff);
            header[7] = (byte) ((totalDataLen >> 24) & 0xff);
            header[8] = 'W';
            header[9] = 'A';
            header[10] = 'V';
            header[11] = 'E';
            header[12] = 'f'; // 'fmt ' chunk
            header[13] = 'm';
            header[14] = 't';
            header[15] = ' ';
            header[16] = 16; // 4 bytes: size of 'fmt ' chunk
            header[17] = 0;
            header[18] = 0;
            header[19] = 0;
            header[20] = 1; // format = 1
            header[21] = 0;
            header[22] = (byte) channels;
            header[23] = 0;
            header[24] = (byte) (longSampleRate & 0xff);
            header[25] = (byte) ((longSampleRate >> 8) & 0xff);
            header[26] = (byte) ((longSampleRate >> 16) & 0xff);
            header[27] = (byte) ((longSampleRate >> 24) & 0xff);
            header[28] = (byte) (byteRate & 0xff);
            header[29] = (byte) ((byteRate >> 8) & 0xff);
            header[30] = (byte) ((byteRate >> 16) & 0xff);
            header[31] = (byte) ((byteRate >> 24) & 0xff);
            header[32] = (byte) (2 * 16 / 8); // block align
            header[33] = 0;
            header[34] = RECORDER_BPP; // bits per sample
            header[35] = 0;
            header[36] = 'd';
            header[37] = 'a';
            header[38] = 't';
            header[39] = 'a';
            header[40] = (byte) (totalAudioLen & 0xff);
            header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
            header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
            header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

            out.write(header, 0, 44);
        }
    }

    /**
     *author Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
     * La fonction getAmplitude permet de récupérer les amplitudes en fonction du temps d'un fichier wav ayant à raison de 1 amplitude toute les 1ms  pour les wav ayant un taux d'échantillionnage de 44100
     * @param path Correspond au chemin vers le fichier wav visé, de type String
     * @return Retourne un Vecteur de Double contenant les amplitudes en fonction du temps
     */
    public static Vector<Double> getAmplitude(Context context, String path, boolean filtre)
    {
        try
        {
            // Open the Wav file specified as the first argument
            Wav.WavData wavFile = Wav.WavData.openWavFile(new File(path));

            // Display information about the Wav file
            wavFile.display();

            // Get the number of audio channels in the Wav file
            int numChannels = wavFile.getNumChannels();

            // Create a buffer of 100 frames
            double[] buffer = new double[100 * numChannels];

            int framesRead;

            Vector<Double> data = new Vector<>();
            Vector<Double> amplitude = new Vector<>();

            do
            {
                // Read frames into buffer
                framesRead = wavFile.readFrames(buffer, 100);
                // Loop through frames and look for minimum and maximum value
                for (int s=0 ; s<framesRead * numChannels ; s++)
                {
                    data.add(buffer[s]);
                }
            }
            while (framesRead != 0);

            // Close the wavFile
            wavFile.close();

            int i = 0;
            while(i<data.size())
            {
                if(amplitude.size() == 1000 * ((int) 1 + i/(rate * 2))){// *2 est du a la stereo
                    i = (rate * 2) * (1 + i/(rate * 2));
                }
                if(i%(2*rate/1000) == 0)//1 données toute les 44 valeurs = 1 données toute les 1 ms
                {
                    amplitude.add(data.get(i));
                }
                i++;
            }

            if(filtre) {
                Filter highPassFilter = new Filter(context.getResources().getInteger(R.integer.highPassFilter), rate, Filter.PassType.HighPass, Double.parseDouble(context.getResources().getString(R.string.highPassResonance)));
                Filter lowPassFilter = new Filter(context.getResources().getInteger(R.integer.lowPassFilter), rate, Filter.PassType.LowPass, Double.parseDouble(context.getResources().getString(R.string.lowPassResonance)));
                for (int y = 0; y < amplitude.size(); y++) {
                    highPassFilter.Update(amplitude.get(y));
                    lowPassFilter.Update(highPassFilter.getValue());
                    amplitude.set(y, lowPassFilter.getValue());
                }
            }
            return amplitude;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static Vector<Double> getAmplitude(Context context, String path){ return getAmplitude(context, path, false);  }

    public static double getF0(Vector<Double> amplitude) {
        long autoCorrelationMax = 0;
        int periodRef = 0;
        long autoCorrelation;

        for (int i = HzToPeriod(400); i < HzToPeriod(40); i++) {
            autoCorrelation = autoCorrelation(i, amplitude);
            if (autoCorrelation > autoCorrelationMax * 1.03) {
                autoCorrelationMax = autoCorrelation;
                periodRef = i;
            }
        }
        return rate / periodRef;
    }

    private static int HzToPeriod(int Hz) {
        return rate / Hz;
    }

    private static long autoCorrelation(int lag, Vector<Double> data) {
        long integralSum = 0;

        //calculate sum / integral (for given lag and t0)
        for ( int t = 0; t < data.size() - lag; t++ ) {
            integralSum += data.get( t ) * data.get( t + lag );
        }

        return integralSum;
    }
}