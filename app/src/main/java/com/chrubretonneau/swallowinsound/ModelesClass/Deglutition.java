package com.chrubretonneau.swallowinsound.ModelesClass;

import android.content.Context;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoDeglutition;

/**
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class Deglutition {
    private int id;
    private int timeBeginDeglutition;
    private int timeEndDeglutition;

    /**
     * Constructeur par défaut
     */
    public Deglutition() {
    }

    /**
     * Constructeur lorsque toute les données sont connues
     *
     * @param unTimeBeginDeglutition Le temps en milisecondes auquel commence la déglutition, de type int
     * @param unTimeEndDeglutition   Le temps en milisecondes auquel fini la déglutition, de type  int
     */
    public Deglutition(int unTimeBeginDeglutition, int unTimeEndDeglutition) {

        timeBeginDeglutition = unTimeBeginDeglutition;
        timeEndDeglutition = unTimeEndDeglutition;
    }

    /**
     * Constructeur lorsque toute les données sont connues
     *
     * @param id L'identifiant de la déglutition en base de données
     * @param unTimeBeginDeglutition Le temps en milisecondes auquel commence la déglutition, de type int
     * @param unTimeEndDeglutition   Le temps en milisecondes auquel fini la déglutition, de type  int
     */
    public Deglutition(int id, int unTimeBeginDeglutition, int unTimeEndDeglutition) {
        this.id = id;
        timeBeginDeglutition = unTimeBeginDeglutition;
        timeEndDeglutition = unTimeEndDeglutition;
    }

    @Override
    public String toString() {
        return "begin : " + timeBeginDeglutition + "ms, End : " + timeEndDeglutition + "ms";
    }

    public int getTimeBeginDeglutition() {
        return timeBeginDeglutition;
    }

    public void setTimeBeginDeglutition(int unTimeBeginDeglutition) {
        timeBeginDeglutition = unTimeBeginDeglutition;
    }

    public int getTimeEndDeglutition() {
        return timeEndDeglutition;
    }

    public void setTimeEndDeglutition(int unTimeEndDeglutition) {
        timeEndDeglutition = unTimeEndDeglutition;
    }

    public int getIdDeglutition(){
        return id;
    }

    /**
     * Permet de sauvegarder dans la base de données la déglutition
     *
     * @param idExamen  L'identifiant de l'examen auquel appartient la déglutition , de type Integer
     * @param idAliment L'identifiant de l'aliment auquel appartient la déglutition, de type Integer
     * @param context   Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Conetxt
     * @return Retourne un Boolean correspondant au statut de la fonction : true si l'insertion s'est correctement déroulée ; false si l'insertion a échoué
     */
    public boolean save(int idExamen, int idAliment, Context context) {
        DaoDeglutition bdd = new DaoDeglutition(context);

        try {
            bdd.insertDeglutition(timeBeginDeglutition, timeEndDeglutition, idAliment, idExamen);
            bdd.close();

            return true;
        } catch (Exception e) {
            bdd.close();
            return false;
        }
    }
}
