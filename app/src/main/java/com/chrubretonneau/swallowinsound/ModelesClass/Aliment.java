package com.chrubretonneau.swallowinsound.ModelesClass;


/**
 * The object Id should be only define when the object is inserted in database
 * <p>
 * Developped by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 */
public class Aliment {

    /**
     * Should be only define when the object is inserted in database
     */
    private int idAliment;

    private String libelle;

    /**
     * param "n"
     */
    private int param1;

    /**
     * param "dividende"
     */
    private int param2;

    /**
     * Constructor with idAliment
     *
     * @param name
     * @param param1
     * @param param2
     */
    public Aliment(int id, String name, int param1, int param2) {
        this.idAliment = id;
        this.libelle = name;
        this.param1 = param1;
        this.param2 = param2;
    }

    /**
     * Constructor without idAliment
     *
     * @param name
     * @param param1
     * @param param2
     */
    public Aliment(String name, int param1, int param2) {
        this.libelle = name;
        this.param1 = param1;
        this.param2 = param2;
    }

    public int getIdAliment() {
        return idAliment;
    }

    public void setIdAliment(int idAliment) {
        this.idAliment = idAliment;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getParam1() {
        return param1;
    }

    public void setParam1(int param1) {
        this.param1 = param1;
    }

    public int getParam2() {
        return param2;
    }

    public void setParam2(int param2) {
        this.param2 = param2;
    }
}
