package com.chrubretonneau.swallowinsound.ModelesClass;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoPathologie;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoPatient;

import java.util.Calendar;

/**
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class Patient implements Parcelable {
    private int idPatient;
    private String nom;
    private String prenom;
    private String birthday;
    private String pathologie;
    private int idExaminateur;

    /**
     * Constructeur par defaut
     */
    public Patient() {
    }

    /**
     * Constructeur lorsque toute les données sont connues
     *
     * @param unIdPatient     L'identifiant du patient, de type Integer
     * @param unNom           Le nom du patient, de type String
     * @param unPrenom        Le prénom du patient, de type String
     * @param uneBirthday     La date de naissance du patient au format jj-mm-aaaa, de type String
     * @param unePathologie   La pathologie du patient, de type String
     * @param unIdExaminateur L'identifiant de l'examinateur en charge du patient, de type Integer
     */
    public Patient(int unIdPatient, String unNom, String unPrenom, String uneBirthday, String unePathologie, int unIdExaminateur) {
        idPatient = unIdPatient;
        nom = unNom;
        prenom = unPrenom;
        birthday = uneBirthday;
        pathologie = unePathologie;
        idExaminateur = unIdExaminateur;
    }

    /**
     * Constructeur lorsque seul le nom et prénom du patient sont connus
     *
     * @param patronyme Le patronyme correspond au nom et prénom du patient sous la forme "prénom nom" , de type String
     * @param context   Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     */
    public Patient(String patronyme, Context context) {
        DaoPatient bdd = new DaoPatient(context);
        Patient p = bdd.getPatient(patronyme);
        bdd.close();

        idPatient = p.getIdPatient();
        nom = p.getNom();
        prenom = p.getPrenom();
        birthday = p.getBirthday();
        pathologie = p.getPathologie();
        idExaminateur = p.getIdExaminateur();
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int unIdPatient) {
        idPatient = unIdPatient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String unNom) {
        nom = unNom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String unPrenom) {
        prenom = unPrenom;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String uneBirthday) {
        birthday = uneBirthday;
    }

    public String getPathologie() {
        return pathologie;
    }

    public void setPathologie(String unePathologie) {
        pathologie = unePathologie;
    }

    public int getIdExaminateur() {
        return idExaminateur;
    }

    public void setIdExaminateur(int unIdExaminateur) {
        idExaminateur = unIdExaminateur;
    }

    /**
     * Fonction qui permet de determiner l'age selon la date de naissance du patient
     *
     * @return Retourne un Integer correspondant à l'age du patient
     */
    public int getAge() {
        int age;
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        String[] naissance = birthday.split("-");
        age = year - Integer.parseInt(naissance[2]);

        if (month - Integer.parseInt(naissance[1]) < 0) {
            age--;
        } else if (month == Integer.parseInt(naissance[1])) {
            if (day - Integer.parseInt(naissance[0]) < 0) {
                age--;
            }
        }
        return age;
    }

    /**
     * Permet de sauvegarder dans la base de données le patient
     *
     * @param context Context dans lequel est appelé le constructeur (activité en cours par exemple) , de type Context
     * @return Retourne un Integer correspondant au statut de la fonction : -1 si le patient existe déjà dans la base ; id attribué au patient à son insertion ; -2 si l'insertion a échoué
     */
    public int save(Context context) {
        DaoPatient bddPatient = new DaoPatient(context);
        int alreadyExist = bddPatient.getIdPatient(nom, prenom);
        if (alreadyExist == -1) {
            DaoPathologie bddPathologie = new DaoPathologie(context);
            int idPatho = bddPathologie.getIdPathologie(pathologie);
            bddPathologie.close();
            String rqt = "insert into Patients (nom, prenom, birthday, idPathologie, idExaminateur) values ('" + nom + "', '" + prenom + "', '" + birthday + "', " + idPatho + ", '" + idExaminateur + "')";

            if (bddPatient.insertPatient(nom, prenom, birthday, idPatho, idExaminateur) == true) {
                int idPatient = bddPatient.getMaxIdPatient();
                bddPatient.close();
                return idPatient;
            } else {
                return -2;
            }
        }

        return alreadyExist;
    }


    /*------------------------- Parcelable -------------------------*/
    @Override
    public int describeContents() {
        return 0;
    }

    protected Patient(Parcel in) {
        idPatient = in.readInt();
        idExaminateur = in.readInt();
        nom = in.readString();
        prenom = in.readString();
        birthday = in.readString();
        pathologie = in.readString();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idPatient);
        dest.writeInt(this.idExaminateur);
        dest.writeString(this.nom);
        dest.writeString(this.prenom);
        dest.writeString(this.birthday);
        dest.writeString(this.pathologie);
    }
}
