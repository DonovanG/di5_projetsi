package com.chrubretonneau.swallowinsound.ModelesClass;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

public class Permission {

    public final static String[] permissionList = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
//            Manifest.permission.WRITE_STORAGE, // old
//            Manifest.permission.READ_STORAGE, //old
    };

    /**
     * Check if all permission are granted
     * need an activity to get the context
     *
     * @return true if all permission are granted, else false
     */
    public static boolean hasAllPermission(Activity activity) {
        boolean hasAllPermission = true;
        for (String permission : permissionList) {
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                hasAllPermission = false;
            }
        }
        return hasAllPermission;
    }

}
