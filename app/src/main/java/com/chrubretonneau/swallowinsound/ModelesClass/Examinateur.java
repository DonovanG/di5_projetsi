package com.chrubretonneau.swallowinsound.ModelesClass;

import android.content.Context;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExaminateur;

/**
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class Examinateur {
    private int idExaminateur;
    private String nom;

    /**
     * Constructeur par defaut
     */
    public Examinateur() {
    }

    /**
     * Constructeur lorsque toute les données sont connues
     *
     * @param unIdExaminateur L'identifiant de l'examinateur, de type Integer
     * @param unNom           Le nom de l'examinateur, de type String
     */
    public Examinateur(int unIdExaminateur, String unNom) {
        idExaminateur = unIdExaminateur;
        nom = unNom;
    }

    /**
     * Constructeur lorsque seul l'identifiant est connu
     *
     * @param unIdExaminateur L'identifiant de l'examinateur, de type Integer
     * @param context         Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     */
    public Examinateur(int unIdExaminateur, Context context) {
        DaoExaminateur bdd = new DaoExaminateur(context);

        Examinateur ex = bdd.getExaminateur(unIdExaminateur);
        bdd.close();

        idExaminateur = unIdExaminateur;
        nom = ex.getNom();
    }

    public int getIdExaminateur() {
        return idExaminateur;
    }

    public void setIdExaminateur(int unIdExaminateur) {
        idExaminateur = unIdExaminateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String unNom) {
        nom = unNom;
    }

    /**
     * Permet de sauvegarder dans la base de données l'examinateur
     *
     * @param context Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     * @return Retourne un Boolean correspondant au statut de la fonction : true l'examinateur a bient été enregistré ; false si l'insertion a échoué
     */
    public boolean save(Context context) {
        DaoExaminateur bdd = new DaoExaminateur(context);


        if (bdd.insertExaminateur(idExaminateur, nom) == true) {
            return true;
        } else {
            return false;
        }
    }
}
