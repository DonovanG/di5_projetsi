package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chrubretonneau.swallowinsound.ModelesClass.Examen;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoExamen extends DaoAbstractBase {

    public DaoExamen(Context context) {
        super(context);
    }

    /**
     * La fonction getExamen permet de récupérer un examen lorsque l'on connaît son identifiant
     *
     * @param idExamen Correspond à l'identifiant de l'examen recherché, de type Integer
     * @return Retourne un Examen possèdant l'identifiant passé en paramètre
     */
    public Examen getExamen(int idExamen) {
        String date = "";
        int idPatient = -1;
        String[] quantitees = new String[3];
        String[] durees = new String[4];
        int[] nbDeglu = new int[4];
        String[] dureeMoyenneDeglu = new String[4];
        String[] dureeMoyenneInter = new String[4];
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT quantite, duree, nbDeglu, dureeMoyenneDeglu, dureeMoyenneInter FROM Manger WHERE idExamen = " + idExamen + " ORDER BY idAliment", null);

        if (c.moveToFirst()) {
            int aliment = 0;
            do {
                quantitees[aliment] = c.getString(0);
                durees[aliment + 1] = c.getString(1);
                nbDeglu[aliment + 1] = c.getInt(2);
                dureeMoyenneDeglu[aliment + 1] = c.getString(3);
                dureeMoyenneInter[aliment + 1] = c.getString(4);

                aliment++;

            } while (c.moveToNext());
        }
        c.close();

        c = db.rawQuery("SELECT idPatient, dateExamen, dureeExamen, nbDegluTT, dureeMoyenneDegluTT, dureeMoyenneInterTT FROM Examens WHERE idExamen = " + idExamen, null);

        if (c.moveToFirst()) {
            idPatient = c.getInt(0);
            date = c.getString(1);
            durees[0] = c.getString(2);
            nbDeglu[0] = c.getInt(3);
            dureeMoyenneDeglu[0] = c.getString(4);
            dureeMoyenneInter[0] = c.getString(5);
        }
        c.close();
        db.close();

        Examen examen = new Examen(idExamen, idPatient, date, quantitees, durees, nbDeglu, dureeMoyenneDeglu, dureeMoyenneInter);

        return examen;
    }


    /**
     * La fonction getIdExamen permet de récupérer l'identifiant d'un examen lorsque l'on connaît l'identifiant du patient et la date de l'examen
     *
     * @param idPatient  Correspond à l'identifiant du patient, de type Integer
     * @param dateExamen Correspond à la date à laquelle a au lieu l'examen, de type String
     * @return Retourne un Integer correspondant à l'identifiant de l'examen si celui à été retrouvé dans la base de données et à -1 sinon
     */
    public int getIdExamen(int idPatient, String dateExamen) {
        int idExamen;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT idExamen FROM Examens WHERE idPatient = " + idPatient + " AND dateExamen = '" + dateExamen + "'", null);

        c.moveToFirst();
        if (c.getCount() == 0) {
            idExamen = -1;
        } else {
            idExamen = Integer.parseInt(c.getString(0));
        }
        c.close();
        db.close();

        return idExamen;
    }

    /**
     * La fonction getMaxIdExamen permet de récupérer l'identifiant le plus élevé des exammens
     *
     * @return Retourne un Integer correspondant à l'identifiant le plus élevé des examens et à -1 si une erreur s'est produite
     */
    public int getMaxIdExamen() {
        int idExamen;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT MAX(idExamen) FROM Examens", null);
        try {
            c.moveToFirst();
            idExamen = Integer.parseInt(c.getString(0));
        } catch (Exception e) {
            idExamen = -1;
        }
        c.close();
        db.close();

        return idExamen;
    }

    /**
     * La fonction LoadHomepage permet de récupérer tout les examens qui ont été réalisés par ordre chronologique inverse
     *
     * @return Retourne une ArrayList contenant des HashMap<String, String>, contenant eux même les informations d'un examen, a savoir :
     * _ L'identifiant de l'examen : key = idExamen
     * _ Le prenom et nom du patient : key = Patient
     * _ La date de l'examen : key = Date
     * _ le prenom et nom de l'examinateur : key = Examinateur
     * _ La durée de l'examen : key = Duree
     */
    public ArrayList<HashMap<String, String>> getExamenList() {
        HashMap<String, String> examen;
        ArrayList<HashMap<String, String>> examens = new ArrayList<HashMap<String, String>>();

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor c = db.rawQuery("Select idExamen, P.prenom, P.nom, dateExamen, Et.nom, dureeExamen " +
                    "From Patients P, Examinateurs Et, Examens E " +
                    "Where Et.idExaminateur = P.idExaminateur " +
                    "And P.idPatient = E.idPatient " +
                    "Order by idExamen desc", null);

            if (c.moveToFirst()) {
                do {
                    examen = new HashMap<String, String>();
                    examen.put("idExamen", c.getString(0));
                    examen.put("Patient", c.getString(1) + " " + c.getString(2));
                    examen.put("Date", c.getString(3));
                    examen.put("Examinateur", c.getString(4));
                    examen.put("Duree", c.getString(5));

                    examens.add(examen);
                } while (c.moveToNext());
            }
            c.close();

            db.close();


            return examens;
        } catch (Exception e) {
            System.out.println("erreur lors de la récupération des examens pour la page principale : " + e);
        }
        return examens;
    }


    /**
     * La fonction deleteExamen permet de supprimer en examen via son identifiant
     *
     * @param idExamen Correspond à l'identifiant de l'examen voulant être supprimé, de type Integer
     */
    public boolean deleteExamen(int idExamen) {
        try {
            boolean result = deleteQuery("Delete From Examens Where idExamen = " + idExamen);

            if (result == true) {

                result = deleteQuery("Delete From Deglutitions Where idExamen = " + idExamen);

                if (result == true) {
                    result = deleteQuery("Delete From Manger Where idExamen = " + idExamen);
                }
            }
            return result;
        } catch (Exception e) {
            System.out.println("erreur lors de la récupération des examens pour la page principale : " + e);
            return false;
        }
    }


    /**
     * La fonction insertExamun permet d'ajouter un nouvel examun en base de données
     *
     * @param idPatient          Correspond à l'identifiant du Patient lié, de type Integer
     * @param date               Correspond à la date de l'examen
     * @param duree              Correspond à la durée de l'examen
     * @param nbDeglu            Coorespond au nombre de déglutitions
     * @param dureesMoyenneDeglu Correspond à la durée moyenne des déglutitions
     * @param dureesMoyenneInter Correspond à la durée moyenne entre les déglutitions
     * @return
     */
    public boolean insertExamen(int idPatient, String date, String duree, int nbDeglu, String dureesMoyenneDeglu, String dureesMoyenneInter) {
        return insertQuery("insert into Examens (idPatient, dateExamen, dureeExamen, nbDegluTT, dureeMoyenneDegluTT, dureeMoyenneInterTT) values ('" + idPatient + "', '" + date + "', '" + duree + "', " + nbDeglu + ", '" + dureesMoyenneDeglu + "', '" + dureesMoyenneInter + "')");
    }

    /**
     * La fonction updateExamen permet de mettre à jour un examen
     *
     * @param idExamen           Correspond à l'identifiant de l'examen lié, de type Integer
     * @param quantitee          Correspond à la quantité de l'examen
     * @param duree              Correspond à la durée de l'examen
     * @param nbDeglu            Coorespond au nombre de déglutitions
     * @param dureesMoyenneDeglu Correspond à la durée moyenne des déglutitions
     * @param dureesMoyenneInter Correspond à la durée moyenne entre les déglutitions
     */
    public boolean updateExamen(int idExamen, String quantitee, String duree, int nbDeglu, String dureesMoyenneDeglu, String dureesMoyenneInter) {
        return updateQuery("Update Examens SET dureeExamen = '" + duree + "', nbDegluTT = " + nbDeglu + ", dureeMoyenneDegluTT = '" + dureesMoyenneDeglu + "', dureeMoyenneInterTT = '" + dureesMoyenneInter + "' Where idExamen = " + idExamen);
    }

    /**
     * La fonction updateManger permet d'ajouter une étape d'un examen
     *
     * @param idExamen           Correspond à l'identifiant de l'examen lié, de type Integer
     * @param quantitee          Correspond à la quantité de l'examen
     * @param duree              Correspond à la durée de l'examen
     * @param nbDeglu            Coorespond au nombre de déglutitions
     * @param dureesMoyenneDeglu Correspond à la durée moyenne des déglutitions
     * @param dureesMoyenneInter Correspond à la durée moyenne entre les déglutitions
     */
    public void insertManger(int idExamen, String quantitee, String duree, int nbDeglu, String dureesMoyenneDeglu, String dureesMoyenneInter) {
        insertQuery("insert into Manger values(" + idExamen + ", 1, '" + quantitee + "', '" + duree + "', " + nbDeglu + ", '" + dureesMoyenneDeglu + "', '" + dureesMoyenneInter + "')");
    }


    /**
     * La fonction updateManger permet de mettre à jour une étape d'un examen
     *
     * @param idExamen           Correspond à l'identifiant de l'examen lié, de type Integer
     * @param quantitee          Correspond à la quantité de l'examen
     * @param duree              Correspond à la durée de l'examen
     * @param nbDeglu            Coorespond au nombre de déglutitions
     * @param dureesMoyenneDeglu Correspond à la durée moyenne des déglutitions
     * @param dureesMoyenneInter Correspond à la durée moyenne entre les déglutitions
     */
    public boolean updateManger(int idExamen, String quantitee, String duree, int nbDeglu, String dureesMoyenneDeglu, String dureesMoyenneInter, int idAliment) {
        return updateQuery("Update Manger SET quantite = '" + quantitee + "', duree = '" + duree + "', nbDeglu = '" + nbDeglu + "', dureeMoyenneDeglu = '" + dureesMoyenneDeglu + "', dureeMoyenneInter = '" + dureesMoyenneInter + "' Where idExamen = " + idExamen + " And idAliment = "+idAliment);
    }


}
