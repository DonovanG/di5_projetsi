package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chrubretonneau.swallowinsound.ModelesClass.Aliment;
import com.chrubretonneau.swallowinsound.ModelesClass.Deglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Examen;
import com.chrubretonneau.swallowinsound.ModelesClass.Examinateur;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;
import com.chrubretonneau.swallowinsound.Utilities.Analyse;
import com.chrubretonneau.swallowinsound.home;
import com.chrubretonneau.swallowinsound.repas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoDeglutition extends DaoAbstractBase {

    Context context;

    public DaoDeglutition(Context context) {
            super(context);
            this.context = context;
        }


        /**
         * La fonction getListeDeglutition permet de récupérer toute les déglutitions d'un aliment lors d'un examen
         *
         * @param idExamen  Correspond à l'identifiant de l'examen recherché, de type Integer
         * @param idAliment Correspond à l'identifiant de l'aliment visé, de type Integer
         * @return Retourne une List de Deglutition contenant toute les déglutitions de l'aliment visé
         */
        public List<Deglutition> getListeDeglutition(int idExamen, int idAliment) {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT idDeglutition, timeBeginDeglutition, timeEndDeglutition " +
                    "FROM Deglutitions D, Aliments A " +
                    "WHERE D.idAliment = A.idAliment AND idExamen = " + idExamen + " AND A.idAliment = " + idAliment +
                    " ORDER BY timeBeginDeglutition ASC", null);

        List<Deglutition> listeDeglutitions = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Deglutition deglu = new Deglutition(Integer.parseInt(c.getString(0)), Integer.parseInt(c.getString(1)), Integer.parseInt(c.getString(2)));
                listeDeglutitions.add(deglu);

            } while (c.moveToNext());
        }

        db.close();

        return listeDeglutitions;
    }


    /**
     * La fonction deleteDeglutition permet de supprimer la déglution liée à un examen
     *
     * @param idExamen Correspond à l'identifiant de l'examen recherché, de type Integer
     */
    public void deleteDeglutition(int idExamen) {
        deleteQuery("Delete from Deglutitions where idExamen = " + idExamen);
    }

    /**
     * La fonction deleteDeglutition permet de supprimer la déglution liée à un examen
     *
     * @param id Correspond à l'identifiant de la déglutition à supprimer, de type Integer
     */
    public void deleteDeglutitionById(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT idExamen FROM Deglutitions  where idDeglutition = " + id, null);

        c.moveToFirst();
        int idExamen =  c.getInt(0);

        c.close();
        db.close();

        deleteQuery("Delete from Deglutitions where idDeglutition = " + id);

        updateStatistiques(idExamen);

    }




    /**
     * La fonction insertDeglutition permet d'ajouter une nouvelle déglutition en base de données
     *
     * @param timeBeginDeglutition Correspond à la date de début
     * @param timeEndDeglutition   Correspond à la date de fin
     * @param idAliment            Correspond à l'identifiant de l'aliment lié, de type Integer
     * @param idExamen             Correspond à l'identifiant de l'examen lié, de type Integer
     */
    public void insertDeglutition(int timeBeginDeglutition, int timeEndDeglutition, int idAliment, int idExamen) {
        insertQuery("Insert into Deglutitions(timeBeginDeglutition, timeEndDeglutition, idAliment, idExamen) Values(" + timeBeginDeglutition + ", " + timeEndDeglutition + ", " + idAliment + ", " + idExamen + ")");
        updateStatistiques(idExamen);
    }


    public void updateStatistiques(int idExamen){
        long[] dureesMS = new long[3];
        List<List<Deglutition>> listesDeglutitions = new ArrayList<>();

        /** on récupère les déglutitions à évaluer **/
        List<Deglutition> listeDeglutitionsPuree = getListeDeglutition(idExamen,1);
        List<Deglutition> listeDeglutitionsEau = getListeDeglutition(idExamen,2);
        List<Deglutition> listeDeglutitionsYaourt = getListeDeglutition(idExamen,3);

        /** on calcul les durée de début et de fin d'enregistrement **/
        if(!listeDeglutitionsPuree.isEmpty()) {
            dureesMS[0] = (long) (listeDeglutitionsPuree.get(listeDeglutitionsPuree.size() - 1).getTimeEndDeglutition() - listeDeglutitionsPuree.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[0] = 0;
        }

        if(!listeDeglutitionsEau.isEmpty()) {
            dureesMS[1] = (long) (listeDeglutitionsEau.get(listeDeglutitionsEau.size() - 1).getTimeEndDeglutition() - listeDeglutitionsEau.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[1] = 0;
        }

        if(!listeDeglutitionsYaourt.isEmpty()) {
            dureesMS[2] = (long) (listeDeglutitionsYaourt.get(listeDeglutitionsYaourt.size() - 1).getTimeEndDeglutition() - listeDeglutitionsYaourt.get(0).getTimeBeginDeglutition());
        }
        else {
            dureesMS[2] = 0;
        }

        /** on créer une liste globale **/
        listesDeglutitions.add(listeDeglutitionsPuree);
        listesDeglutitions.add(listeDeglutitionsEau);
        listesDeglutitions.add(listeDeglutitionsYaourt);

        /** on calcul les statistiques de l'enregistrement **/
        HashMap<String, Object> statistiques = Analyse.getStatistics(dureesMS, listesDeglutitions);
        DaoExamen daoExamen = new DaoExamen(context);

        /** Update examen **/
        Examen oldExamen = daoExamen.getExamen(idExamen);
        Examen examen = new Examen(oldExamen.getIdPatient(), oldExamen.getDate(), oldExamen.getQuantitees(), (String[]) statistiques.get("Durees"), (int[]) statistiques.get("NbDeglus"), (String[]) statistiques.get("DureeMoyenneDeglu"), (String[]) statistiques.get("DureeMoyenneInterDeglu"), context);

//        idExamen = examen.update(context);


    }
}
