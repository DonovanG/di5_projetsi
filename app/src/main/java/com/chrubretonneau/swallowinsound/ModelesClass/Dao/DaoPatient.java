package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chrubretonneau.swallowinsound.ModelesClass.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoPatient extends DaoAbstractBase {

    public DaoPatient(Context context) {
        super(context);
    }

    /**
     * La fonction getIdPatient permet de récupérer l'identifiant d'un patient lorsque l'on connaît son nom et prenom
     *
     * @param nom    Correspond au nom du patient, de type String
     * @param prenom Correspond au prenom du patient, de type String
     * @return Retourne un Integer correspondant à l'identifiant du patient si celui à été retrouvé dans la base de données et à -1 sinon
     */
    public int getIdPatient(String nom, String prenom) {
        int idPatient;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT idPatient FROM Patients Where nom = '" + nom + "' and prenom = '" + prenom + "'", null);

        c.moveToFirst();
        if (c.getCount() == 0) {
            idPatient = -1;
        } else {
            idPatient = Integer.parseInt(c.getString(0));
        }
        c.close();
        db.close();

        return idPatient;
    }

    /**
     * La fonction getMaxIdPatient permet de récupérer l'identifiant le plus élevé des patients
     *
     * @return Retourne un Integer correspondant à l'identifiant le plus élevé des patients et à -1 si une erreur s'est produite
     */
    public int getMaxIdPatient() {
        int idPatient;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT MAX(idPatient) FROM Patients", null);

        try {
            c.moveToFirst();
            idPatient = Integer.parseInt(c.getString(0));
        } catch (Exception e) {
            idPatient = -1;
        }
        c.close();
        db.close();

        return idPatient;
    }


    /**
     * La fonction getPatient permet de récupérer un patient lorsque l'on connaît son nom et prenom
     *
     * @param patronyme Le patronyme correspond au nom et prénom du patient sous la forme "prénom nom" , de type String
     * @return Retourne un Patient portant le patronyme passé en paramètre si celui à été retrouvé dans la base de données et retourne null sinon
     */
    public Patient getPatient(String patronyme) {
        String[] prenom_nom = patronyme.split(" ");
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM Patients Where nom = '" + prenom_nom[1] + "' and prenom = '" + prenom_nom[0] + "'", null);

        try {
            c.moveToFirst();
            Patient patient = new Patient(Integer.parseInt(c.getString(0)), c.getString(1), c.getString(2), c.getString(3), c.getString(4), Integer.parseInt(c.getString(5)));
            c.close();
            db.close();

            return patient;
        } catch (Exception ex) {
            System.out.println("Erreur : " + ex);
            return null;
        }
    }

    /**
     * La fonction getAllPatronymePatient permet de récupérer tout les patronymes ("prenom nom") des patients
     *
     * @return Retourne une List de String contenant tout les patronymes
     */
    public List<String> getAllPatronymePatient() {
        List<String> patronymes = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT prenom, nom FROM Patients ", null);

        if (c.moveToFirst()) {
            do {
                patronymes.add(c.getString(0) + " " + c.getString(1));

            } while (c.moveToNext());
        }
        c.close();

        db.close();
        return patronymes;
    }

    /**
     * La fonction insertPatient permet d'ajouter un nouveau patient en base de données
     *
     * @param nom           nom Correspond au nom du patient, de type String
     * @param prenom        nom Correspond au prenom du patient, de type String
     * @param birthday      nom Correspond à la date de naissance patient, de type String
     * @param idPatho       Correspond à l'identifiant de la pathologie, de type Integer
     * @param idExaminateur Correspond à l'identifiant de l'examinateur, de type Integer
     * @return
     */
    public boolean insertPatient(String nom, String prenom, String birthday, int idPatho, int idExaminateur) {
        return insertQuery("insert into Patients (nom, prenom, birthday, idPathologie, idExaminateur) values ('" + nom + "', '" + prenom + "', '" + birthday + "', " + idPatho + ", '" + idExaminateur + "')");
    }
}
