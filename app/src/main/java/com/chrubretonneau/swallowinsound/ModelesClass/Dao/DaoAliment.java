package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chrubretonneau.swallowinsound.ModelesClass.Aliment;

import java.util.ArrayList;
import java.util.List;

/**
 * Developped by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 */

public class DaoAliment extends DaoAbstractBase {

    public DaoAliment(Context context) {
        super(context);
    }


    /**
     * La fonction getAllAliments permet de récupérer tous les aliments en bases
     *
     * @return Retourne une List des akmients
     */
    public List<Aliment> getAllAliments() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Aliments", null);

        List<Aliment> liste = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Aliment alim = new Aliment(Integer.parseInt(c.getString(0)),
                        c.getString(1),
                        Integer.parseInt(c.getString(2)),
                        Integer.parseInt(c.getString(3)));
                liste.add(alim);

            } while (c.moveToNext());
        }

        db.close();

        return liste;
    }

    /**
     * La fonction getAliment permet de récupérer un aliment en bases en fonction de son id
     *
     * @Param id de l'aliment
     * @return Retourne l'aliment trouvé, null sinon
     */
    public Aliment getAliment(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Aliments", null);
        if (c.moveToFirst()) {
            do {
                if(Integer.parseInt(c.getString(0)) == id) {
                    Aliment aliment = new Aliment(Integer.parseInt(c.getString(0)),
                            c.getString(1),
                            Integer.parseInt(c.getString(2)),
                            Integer.parseInt(c.getString(3)));
                    db.close();
                    return aliment;
                }

            } while (c.moveToNext());
        }

        db.close();
        return null;
    }

    /**
     * La fonction getAlimentByName permet de récupérer un aliment en bases en fonction de son nom
     *
     * @Param id de l'aliment
     * @return Retourne le premier aliment correspondant au nom, null si aucun n'est trouvé
     */
    public Aliment getAlimentByName(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Aliments", null);
        if (c.moveToFirst()) {
            do {
                if(c.getString(0).equals(name)) {
                    Aliment aliment = new Aliment(Integer.parseInt(c.getString(0)),
                            c.getString(1),
                            Integer.parseInt(c.getString(2)),
                            Integer.parseInt(c.getString(3)));
                    db.close();
                    return aliment;
                }

            } while (c.moveToNext());
        }

        db.close();
        return null;
    }

    /**
     * Return the max id used in aliments table
     *
     * @return maxId
     */
    public int getMaxAlimentId() {
        List<Aliment> list = getAllAliments();
        int maxId = 0;
        for (Aliment aliment : list)
            if (aliment.getIdAliment() > maxId)
                maxId = aliment.getIdAliment();
        return maxId;
    }


    /**
     * La fonction deleteAlim permet de supprimer un aliment
     *
     * @param idAliment Correspond à l'identifiant de l'aliment recherché, de type Integer
     */
    public void deleteAlim(int idAliment) {
        deleteQuery("Delete from Aliments where idAliment = " + idAliment);
    }


    /**
     * La fonction insertAliment permet d'ajouter un nouvel aliment en base de données
     * The id will be set automatically and the object returned with the new id
     *
     * @param aliment Aliment a ajouté
     */
    public Aliment insertAliment(Aliment aliment) {
        aliment.setIdAliment(getMaxAlimentId() + 1);
        insertQuery("Insert into Aliments(idAliment, libelle, param1, param2) Values("
                + aliment.getIdAliment() + ", "
                + aliment.getLibelle() + ", "
                + aliment.getParam1() + ", "
                + aliment.getParam2() + ")");
        return aliment;
    }
}
