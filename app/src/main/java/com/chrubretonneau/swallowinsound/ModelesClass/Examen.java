package com.chrubretonneau.swallowinsound.ModelesClass;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoExamen;

/**
 * author Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class Examen implements Parcelable {

    private int idExamen;
    private int idPatient;
    private String date;
    private String[] quantitees = new String[3];
    private String[] durees = new String[4];
    private int[] nbDeglu = new int[4];
    private String[] dureesMoyenneDeglu = new String[4];
    private String[] dureesMoyenneInter = new String[4];

    /**
     * Constructeur par défaut
     */
    public Examen() {
    }

    /**
     * Constructeur lorsque seul l'identifiant de l'examen est connu
     *
     * @param unIdExamen L'identifiant de l'examen passé, de type Integer
     * @param context    Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     */
    public Examen(int unIdExamen, Context context) {
        DaoExamen bdd = new DaoExamen(context);
        Examen examen = bdd.getExamen(unIdExamen);

        idExamen = examen.getIdExamen();
        idPatient = examen.getIdPatient();
        date = examen.getDate();
        quantitees = examen.getQuantitees();
        durees = examen.getDurees();
        nbDeglu = examen.getNbDeglu();
        dureesMoyenneDeglu = examen.getDureesMoyenneDeglu();
        dureesMoyenneInter = examen.getDureesMoyenneInter();
    }


    /**
     * Constructeur lorsque toute les données sont connues sauf l'identifiant de l'examen
     *
     * @param unIdPatient           L'identifiant du patient passant l'examen, de type Integer
     * @param uneDate               La date à laquelle a lieu l'examen, comprenant l'heure (ex 01-01-2017 12h), de type String
     * @param desQuantitees         La liste des quantités des 3 Aliments, de type String[3]
     * @param desDurees             La liste des durées, total et par aliment
     * @param desNbDeglu            La liste des nombres de déglutition, total et par aliment
     * @param desDureesMoyenneDeglu La liste des durée moyenne de déglutition, total et par aliment
     * @param desDureesMoyenneInter La liste des durée moyenne de inter-déglutition, total et par aliment
     * @param context               Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     */
    public Examen(int unIdPatient, String uneDate, String[] desQuantitees, String[] desDurees, int[] desNbDeglu, String[] desDureesMoyenneDeglu, String[] desDureesMoyenneInter, Context context) {
        DaoExamen bdd = new DaoExamen(context);

        idExamen = bdd.getIdExamen(unIdPatient, uneDate);
        idPatient = unIdPatient;
        date = uneDate;
        quantitees = desQuantitees;
        durees = desDurees;
        nbDeglu = desNbDeglu;
        dureesMoyenneDeglu = desDureesMoyenneDeglu;
        dureesMoyenneInter = desDureesMoyenneInter;
    }


    /**
     * Constructeur lorsque toute les données sont connues
     *
     * @param unIdExamen            L'identifiant de l'examen passé, de type Integer
     * @param unIdPatient           L'identifiant du patient passant l'examen, de type Integer
     * @param uneDate               La date à laquelle a lieu l'examen, comprenant l'heure (ex 01-01-2017 12h), de type String
     * @param desQuantitees         La liste des quantités des 3 Aliments, de type String[3]
     * @param desDurees             La liste des durées, total et par aliment
     * @param desNbDeglu            La liste des nombres de déglutition, total et par aliment
     * @param desDureesMoyenneDeglu La liste des durée moyenne de déglutition, total et par aliment
     * @param desDureesMoyenneInter La liste des durée moyenne de inter-déglutition, total et par aliment
     */
    public Examen(int unIdExamen, int unIdPatient, String uneDate, String[] desQuantitees, String[] desDurees, int[] desNbDeglu, String[] desDureesMoyenneDeglu, String[] desDureesMoyenneInter) {
        idExamen = unIdExamen;
        idPatient = unIdPatient;
        date = uneDate;
        quantitees = desQuantitees;
        durees = desDurees;
        nbDeglu = desNbDeglu;
        dureesMoyenneDeglu = desDureesMoyenneDeglu;
        dureesMoyenneInter = desDureesMoyenneInter;
    }

    public int getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(int unIdExamen) {
        idExamen = unIdExamen;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int unIdPatient) {
        idPatient = unIdPatient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String uneDate) {
        date = uneDate;
    }

    public String[] getQuantitees() {
        return quantitees;
    }

    public void setQuantitees(String[] desQuantitees) {
        quantitees = desQuantitees;
    }

    public String[] getDurees() {
        return durees;
    }

    public void setDurees(String[] desDurees) {
        durees = desDurees;
    }

    public int[] getNbDeglu() {
        return nbDeglu;
    }

    public void setNbDeglu(int[] desNbDeglu) {
        nbDeglu = desNbDeglu;
    }

    public String[] getDureesMoyenneDeglu() {
        return dureesMoyenneDeglu;
    }

    public void setDureesMoyenneDeglu(String[] desDureesMoyenneDeglu) {
        dureesMoyenneDeglu = desDureesMoyenneDeglu;
    }

    public String[] getDureesMoyenneInter() {
        return dureesMoyenneInter;
    }

    public void setDureesMoyenneInter(String[] desDureesMoyenneInter) {
        dureesMoyenneInter = desDureesMoyenneInter;
    }


    /**
     * Permet de sauvegarder dans la base de données l'examen
     *
     * @param context Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     * @return Retourne un Integer correspondant au statut de la fonction : id attribué à l'examin à son insertion ; -1 si l'insertion a échoué
     */
    public int save(Context context) {
        DaoExamen bdd = new DaoExamen(context);

        if (bdd.insertExamen(idPatient, date, durees[0], nbDeglu[0], dureesMoyenneDeglu[0], dureesMoyenneInter[0]) == true) {
            int idExamen = bdd.getMaxIdExamen();
            for (int i = 0; i < 3; i++)
                bdd.insertManger(idExamen, quantitees[i], durees[i], nbDeglu[i], dureesMoyenneDeglu[i], dureesMoyenneInter[i]);

            bdd.close();
            return idExamen;
        } else {
            bdd.close();
            return -1;
        }
    }

    /**
     * Permet de mettre un jour un examen
     *
     * @param context Context dans lequel est appelé le constructeur (activité en cours par exemple), de type Context
     * @return Retourne un Integer correspondant au statut de la fonction : id de l'examin ; -1 si l'insertion a échoué
     */
    public int update(Context context) {
        DaoExamen bdd = new DaoExamen(context);
        int idExamen = bdd.getIdExamen(idPatient, date);


        String quantiteeTT = String.valueOf(Integer.parseInt(quantitees[0]) + Integer.parseInt(quantitees[1]) + Integer.parseInt(quantitees[2]));


        if (bdd.updateExamen(idExamen,quantiteeTT,durees[0],nbDeglu[0],dureesMoyenneDeglu[0],dureesMoyenneInter[0]) == true) {
            if (bdd.updateManger(idExamen,quantitees[0],durees[1],nbDeglu[1],dureesMoyenneDeglu[1],dureesMoyenneInter[1],1) == true) {
                if (bdd.updateManger(idExamen,quantitees[1],durees[2],nbDeglu[2],dureesMoyenneDeglu[2],dureesMoyenneInter[2],2) == true) {
                    if (bdd.updateManger(idExamen,quantitees[2],durees[3],nbDeglu[3],dureesMoyenneDeglu[3],dureesMoyenneInter[3],3) == true) {
                        return idExamen;
                    }
                }

            }
        }

        bdd.close();
        return -1;
    }


    /*------------------------- Parcelable -------------------------*/
    protected Examen(Parcel in) {
        idExamen = in.readInt();
        date = in.readString();
        idPatient = in.readInt();
        quantitees = in.createStringArray();
        durees = in.createStringArray();
        nbDeglu = in.createIntArray();
        dureesMoyenneDeglu = in.createStringArray();
        dureesMoyenneInter = in.createStringArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idExamen);
        dest.writeString(date);
        dest.writeInt(idPatient);
        dest.writeStringArray(quantitees);
        dest.writeStringArray(durees);
        dest.writeIntArray(nbDeglu);
        dest.writeStringArray(dureesMoyenneDeglu);
        dest.writeStringArray(dureesMoyenneInter);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Examen> CREATOR = new Creator<Examen>() {
        @Override
        public Examen createFromParcel(Parcel in) {
            return new Examen(in);
        }

        @Override
        public Examen[] newArray(int size) {
            return new Examen[size];
        }
    };
}
