package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chrubretonneau.swallowinsound.ModelesClass.Examinateur;

import java.util.ArrayList;
import java.util.List;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoExaminateur extends DaoAbstractBase {

    public DaoExaminateur(Context context) {
        super(context);
    }


    /**
     * La fonction getExaminateur permet de récupérer un examinateur lorsque l'on connaît son identifiant
     *
     * @param idExaminateur Correspond à l'identifiant de l'examinateur recherché, de type Integer
     * @return Retourne un Examinateur possèdant l'identifiant passé en paramètre si celui à été retrouvé dans la base de données et retourne null sinon
     */
    public Examinateur getExaminateur(int idExaminateur) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM Examinateurs Where idExaminateur = '" + idExaminateur + "'", null);

        try {
            c.moveToFirst();
            Examinateur examinateur = new Examinateur(Integer.parseInt(c.getString(0)), c.getString(1));
            c.close();
            db.close();

            return examinateur;
        } catch (Exception ex) {
            System.out.println("Erreur " + ex);
            return null;
        }
    }


    /**
     * La fonction getAllExaminateur permet de récupérer tout les examinateurs
     *
     * @return Retourne une List de Examinateur
     */
    public List<Examinateur> getAllExaminateurs() {
        List<Examinateur> examinateurs = new ArrayList<Examinateur>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM Examinateurs ", null);

        if (c.moveToFirst()) {
            do {
                Examinateur him = new Examinateur(Integer.parseInt(c.getString(0)), c.getString(1));
                examinateurs.add(him);

            } while (c.moveToNext());
        }
        c.close();

        db.close();
        return examinateurs;
    }

    /**
     * La focntion insertExaminateur permet d'ajouter un nouvel examinateur en base de données
     *
     * @param idExaminateur Correspond à l'identifiant de l'examinateur, de type Integer
     * @param nom Correspond au nom de l'examinateur, de type String
     */
    public boolean insertExaminateur(int idExaminateur, String nom){
        return insertQuery("insert into Examinateurs values (" + idExaminateur + ", '"+ nom + "')");
    }
}
