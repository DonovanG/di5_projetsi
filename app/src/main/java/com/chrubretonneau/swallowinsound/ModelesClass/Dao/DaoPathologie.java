package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoPathologie extends DaoAbstractBase {

    public DaoPathologie(Context context) {
        super(context);
    }

    /**
     * La fonction getAllPathologies permet de récupérer toute les pathologies
     *
     * @return Retourne une List de String contenant tout les pathologies
     */
    public List<String> getAllPathologies() {
        List<String> pathologies = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT nomPathologie FROM Pathologies ", null);

        if (c.moveToFirst()) {
            do {
                pathologies.add(c.getString(0));

            } while (c.moveToNext());
        }
        c.close();

        db.close();
        return pathologies;
    }

    /**
     * La fonction getIdPathologie permet de récupérer l'identifiant d'une pathologie lorsque l'on connaît son nom
     *
     * @param pathologie Correspond au nom du patient, de type String
     * @return Retourne un Integer correspondant à l'identifiant du patient si celui à été retrouvé dans la base de données et à -1 sinon
     */
    public int getIdPathologie(String pathologie) {
        int idPathologie;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT idPathologie FROM Pathologies Where nomPathologie = '" + pathologie + "'", null);

        c.moveToFirst();
        idPathologie = Integer.parseInt(c.getString(0));
        c.close();
        db.close();

        return idPathologie;
    }

    /**
     * La fonction insertPathologie permet d'ajouter une pathologie à la base
     *
     * @return patho Correspond au nom de la pathologie, de type String
     */
    public boolean insertPathologie(int idPathologie, String patho) {
        return insertQuery("Insert into Pathologies Values(" + idPathologie + ", '" + patho + "')");
    }
}
