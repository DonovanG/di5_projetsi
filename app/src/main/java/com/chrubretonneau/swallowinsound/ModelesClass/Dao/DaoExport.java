package com.chrubretonneau.swallowinsound.ModelesClass.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */

public class DaoExport extends DaoAbstractBase {

    public DaoExport(Context context) {
        super(context);
    }

    /**
     * La fonction export permet d'exporter dans un fichier SQL le contenu de la base de données
     *
     * @return retourne un Boolean correspondant au statut de la fonction : true si l'export s'est déroulé sans problème, false sinon
     */
    public boolean export() {
        boolean result = true;
        SQLiteDatabase bdd = this.getReadableDatabase();

        try {
            FileWriter writer = new FileWriter(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/SwalloWinSound.sql");

            /*-----------------------------Table Examinateurs-----------------------------*/
            Cursor c = bdd.rawQuery("SELECT nom " +
                    "FROM Examinateurs", null);

            if (c.moveToFirst()) {
                writer.append("Examinateurs\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + "\n");
                    writer.flush();
                } while (c.moveToNext());
            }
            c.close();


            /*-----------------------------Table Pathologies-----------------------------*/
            c = bdd.rawQuery("SELECT nomPathologie " +
                    "FROM Pathologies", null);

            if (c.moveToFirst()) {
                writer.append("Pathologies\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + "\n");
                    writer.flush();
                } while (c.moveToNext());
            }
            c.close();


            /*-----------------------------Table Patients-----------------------------*/
            c = bdd.rawQuery("Select idPatient, P.nom, prenom, birthday, nomPathologie, E.nom " +
                    "From Patients P, Examinateurs E, Pathologies Pt " +
                    "Where P.idExaminateur = E.idExaminateur " +
                    "And P.idPathologie = Pt.idPathologie", null);

            if (c.moveToFirst()) {
                writer.append("Patients\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + ";" + c.getString(1) + ";" + c.getString(2) + ";" + c.getString(3) + ";" + c.getString(4) + ";" + c.getString(5) + "\n");
                    writer.flush();
                } while (c.moveToNext());
            }
            c.close();


            /*-----------------------------Table Examens-----------------------------*/
            c = bdd.rawQuery("SELECT nom, prenom, birthday, dateExamen, dureeExamen, nbDegluTT, dureeMoyenneDegluTT, dureeMoyenneInterTT " +
                    "FROM Examens E, Patients P " +
                    "Where E.idPatient = P.idPatient", null);

            if (c.moveToFirst()) {
                writer.append("Examens\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + ";" + c.getString(1) + ";" + c.getString(2) + ";" + c.getString(3) + ";" + c.getString(4) + ";" + c.getString(5) + ";" + c.getString(6) + ";" + c.getString(7) + "\n");
                    writer.flush();

                } while (c.moveToNext());
            }
            c.close();



            /*-----------------------------Table Déglutitions-----------------------------*/
            c = bdd.rawQuery("Select timeBeginDeglutition, timeEndDeglutition, idAliment, nom, prenom, birthday, dateExamen, dureeExamen " +
                    "From Deglutitions D, Patients P, Examens E " +
                    "Where D.idExamen = E.idExamen " +
                    "And E.idPatient = P.idPatient", null);

            if (c.moveToFirst()) {
                writer.append("Deglutitions\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + ";" + c.getString(1) + ";" + c.getString(2) + ";" + c.getString(3) + ";" + c.getString(4) + ";" + c.getString(5) + ";" + c.getString(6) + ";" + c.getString(7) + "\n");
                    writer.flush();

                } while (c.moveToNext());
            }
            c.close();


            /*-----------------------------Table Manger-----------------------------*/
            c = bdd.rawQuery("Select nom, prenom, birthday, dateExamen, dureeExamen, idAliment, quantite, duree, nbDeglu, dureeMoyenneDeglu, dureeMoyenneInter " +
                    "From Manger M, Patients P, Examens E " +
                    "Where M.idExamen = E.idExamen " +
                    "And E.idPatient = P.idPatient", null);

            if (c.moveToFirst()) {
                writer.append("Manger\n");
                writer.flush();

                do {
                    writer.append(c.getString(0) + ";" + c.getString(1) + ";" + c.getString(2) + ";" + c.getString(3) + ";" + c.getString(4) + ";" + c.getString(5) + ";" + c.getString(6) + ";" + c.getString(7) + ";" + c.getString(8) + ";" + c.getString(9) + ";" + c.getString(10) + "\n");
                    writer.flush();

                } while (c.moveToNext());
            }
            c.close();
            writer.close();
        } catch (FileNotFoundException e) {
            result = false;
        } catch (IOException ioex) {
            result = false;
        }
        return result;
    }
}
