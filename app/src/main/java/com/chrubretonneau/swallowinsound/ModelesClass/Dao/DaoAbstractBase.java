package com.chrubretonneau.swallowinsound.ModelesClass.Dao;


/**
 * Refactored by as DATTEE, Polytech Tours Departement Informatique 5ème année - 2019.
 * author  Créé par Tommy Raguideau, Polytech Tours Departement Informatique 5ème année - 2017.
 */


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;


public abstract class DaoAbstractBase extends SQLiteOpenHelper {
    //version number to upgrade database version each time if you Add, Edit table, you need to change the version number.
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "swallowinsound.db";

    public DaoAbstractBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * La fonction onCreate sera appelée au chargement de l'application pour la 1ère fois, on créé les tables de la base de données et on insére les 3 aliments
     *
     * @param db Permet la connexion avec la base de données afin de pouvoir executer les requêtes Create Table et Insert, de type SQLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here
        String ExaminateursTable = "CREATE TABLE Examinateurs (idExaminateur INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL)";
        String PathologieTable = "CREATE TABLE Pathologies (idPathologie INTEGER PRIMARY KEY AUTOINCREMENT, nomPathologie TEXT NOT NULL)";
        String PatientsTable = "CREATE TABLE Patients (idPatient INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, prenom TEXT NOT NULL, birthday TEXT NOT NULL, idPathologie INTEGER NOT NULL, idExaminateur INTEGER NOT NULL, FOREIGN KEY (idExaminateur) REFERENCES Examinateurs(idExaminateur), FOREIGN KEY (idPathologie) REFERENCES Pathologies(idPathologie))";
        String ExamensTable = "CREATE TABLE Examens (idExamen INTEGER PRIMARY KEY AUTOINCREMENT, idPatient INTEGER NOT NULL, dateExamen TEXT NOT NULL, dureeExamen TEXT NOT NULL, nbDegluTT INTEGER NOT NULL, dureeMoyenneDegluTT TEXT NOT NULL, dureeMoyenneInterTT TEXT NOT NULL, FOREIGN KEY (idPatient) REFERENCES Patients(idPatient))";
        String AlimentsTable = "CREATE TABLE Aliments (idAliment INTEGER PRIMARY KEY, libelle TEXT NOT NULL, param1 INTEGER NOT NULL, param2 INTEGER NOT NULL)";
        String DeglutitionsTable = "CREATE TABLE Deglutitions (idDeglutition INTEGER PRIMARY KEY AUTOINCREMENT, timeBeginDeglutition INTEGER NOT NULL, timeEndDeglutition INTEGER NOT NULL, idAliment INTEGER NOT NULL, idExamen INTEGER NOT NULL, FOREIGN KEY (idAliment) REFERENCES Aliments(idAliment), FOREIGN KEY (idExamen) REFERENCES Examens(idExamen))";
        String MangerTable = "CREATE TABLE Manger(idExamen INTEGER, idAliment INTEGER, quantite INTEGER NOT NULL, duree TEXT NOT NULL, nbDeglu INTEGER NOT NULL, dureeMoyenneDeglu TEXT NOT NULL, dureeMoyenneInter TEXT NOT NULL, PRIMARY KEY(idExamen, idAliment), FOREIGN KEY (idExamen) REFERENCES Examens(idExamen), FOREIGN KEY (idAliment) REFERENCES Aliments(idAliment))";

        db.execSQL(ExaminateursTable);
        db.execSQL(PathologieTable);
        db.execSQL(PatientsTable);
        db.execSQL(ExamensTable);
        db.execSQL(AlimentsTable);
        db.execSQL(DeglutitionsTable);
        db.execSQL(MangerTable);

        String insertAliment = "Insert into Aliments Values(1, 'Puree', 125, 13)";
        db.execSQL(insertAliment);

        insertAliment = "Insert into Aliments Values(2, 'Eau', 75, 13)";
        db.execSQL(insertAliment);

        insertAliment = "Insert into Aliments Values(3, 'Yaourt', 75, 9)";
        db.execSQL(insertAliment);
    }

    /**
     * La fonction onUpgrade sera appelé à l'ouverture de l'application : Si la base de données a été mise à jour dans ça structure alors on drop les tables qui existent et on les recréer selon la nouvelle version
     *
     * @param db         Permettre la connexion avec la base de données afin de pouvoir executer les requêtes , de type SQLiteDatabase
     * @param oldVersion Correspond à la version de la base de données lors de l'execution précedente, de type Integer
     * @param newVersion Correspond à la version de la base de données lors de l'execution en cours,  de type Integer
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS Manger");
            db.execSQL("DROP TABLE IF EXISTS Deglutitions");
            db.execSQL("DROP TABLE IF EXISTS Aliments");
            db.execSQL("DROP TABLE IF EXISTS Examens");
            db.execSQL("DROP TABLE IF EXISTS Patients");
            db.execSQL("DROP TABLE IF EXISTS Pathologies");
            db.execSQL("DROP TABLE IF EXISTS Examinateurs");

            // Create tables again
            onCreate(db);
        }
    }

    /**
     * La fonction Insert permet d'insérer des données dans la base de données
     *
     * @param rqt Correspond à la requête SQL Insert que l'on souhaite executer, de type String
     * @return Retourne un Boolean correspond au statut de l'execution de la requête Insert : True si l'insertion s'est bien déroulée, False sinon
     */
    protected boolean insertQuery(String rqt) {
        try {
            //Open connection to write data
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(rqt);
            db.close(); // Closing database connection

            return true;
        } catch (SQLiteException ex) {
            System.out.println(ex);
            return false;
        }
    }

    /**
     * La fonction Update permet de mettre à jours des données dans la base de données
     *
     * @param rqt Correspond à la requête SQL Update que l'on souhaite executer, de type String
     * @return Retourne un Boolean correspond au statut de l'execution de la requête Insert : True si l'insertion s'est bien déroulée, False sinon
     */
    protected boolean updateQuery(String rqt) {
        try {
            //Open connection to write data
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(rqt);
            db.close(); // Closing database connection

            return true;
        } catch (SQLiteException ex) {
            System.out.println(ex);
            return false;
        }
    }

    /**
     * La fonction Delete permet de supprimer des données dans la base de données
     *
     * @param rqt Correspond à la requête SQL Delete que l'on souhaite executer, de type String
     * @return Retourne un Boolean correspond au statut de l'execution de la requête Insert : True si l'insertion s'est bien déroulée, False sinon
     */
    protected boolean deleteQuery(String rqt) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(rqt);
            db.close(); // Closing database connection
            return true;
        } catch (SQLiteException ex) {
            System.out.println(ex);
            return false;
        }
    }

}