package com.chrubretonneau.swallowinsound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chrubretonneau.swallowinsound.ModelesClass.Aliment;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoDeglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Dao.DaoPatient;
import com.chrubretonneau.swallowinsound.ModelesClass.Deglutition;
import com.chrubretonneau.swallowinsound.ModelesClass.Examen;
import com.chrubretonneau.swallowinsound.ModelesClass.Examinateur;
import com.chrubretonneau.swallowinsound.ModelesClass.Patient;
import com.chrubretonneau.swallowinsound.Utilities.Analyse;
import com.chrubretonneau.swallowinsound.soundfile.Wav;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * Created by 21500067t on 25/06/2018.
 */

public final class Test extends Activity {
    private Spinner spPatient, spPuree, spEau, spYaourt, spQPuree, spQEau, spQYaourt;
    private Button btGo;
    private SeekBar sbPercent;
    private TextView txtV;
    private CheckBox checkbox;
    private String dateAndHour;
    private File path;
    private int mode; //0 = Insert ; 1 = Update

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/");
        File[] file = path.listFiles();
        List<String> tmp = new ArrayList<>();
        for (File f : file) {
            if (f.getPath().endsWith(".wav"))
                tmp.add(f.toString());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tmp);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spPuree = (Spinner) findViewById(R.id.spPuree);
        spPuree.setAdapter(adapter);
        spQPuree = (Spinner) findViewById(R.id.spPureeQuantity);

        spEau = (Spinner) findViewById(R.id.spEau);
        spEau.setAdapter(adapter);
        spQEau  =(Spinner) findViewById(R.id.spEauQuantity);

        spYaourt = (Spinner) findViewById(R.id.spYaourt);
        spYaourt.setAdapter(adapter);
        spQYaourt = (Spinner) findViewById(R.id.spYaourtQuantity);

        sbPercent = (SeekBar) findViewById(R.id.seekBar);
        txtV = (TextView) findViewById(R.id.sbTextView);
        btGo = (Button) findViewById(R.id.btGo);
        checkbox = (CheckBox) findViewById(R.id.chkBox);


        try {
            int indexPatient = -1;
            spPatient = (Spinner) findViewById(R.id.spPatient);
            DaoPatient bdd = new DaoPatient(this);
            List<String> patients = bdd.getAllPatronymePatient();
            bdd.close();

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String selectedPatient = patients.get(Integer.parseInt(extras.getString("Source")) - 1);
                Collections.sort(patients);

                int i = 0;
                while (indexPatient == -1 || i < patients.size()) {
                    if (patients.get(i).equals(selectedPatient)) {
                        indexPatient = i;
                    }
                    i++;
                }
            } else {
                Collections.sort(patients);
            }

            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, patients);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            spPatient.setAdapter(dataAdapter);
            if (indexPatient != -1) {
                spPatient.setSelection(indexPatient);
                path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + spPatient.getSelectedItem().toString() + "/" + dateAndHour);
            }

            Calendar c = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH");
            dateAndHour = df.format(c.getTime()) + "h";

            txtV.setText("Ignore Sound peak intensity under: " + sbPercent.getProgress() + "% of maximum intensity");

            sbPercent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                int progress = 0;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                    txtV.setText("Ignore Sound peak intensity under: " + progresValue + "% of maximum intensity");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {  }
            });
        } catch (Exception ex) {
            System.out.println("Erreur : " + ex);
        }

        spPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SwalloWinSound/" + spPatient.getSelectedItem().toString() + "/" + dateAndHour);

                    if (path.exists() && new File(path.getPath() + "/Puree.wav").exists()) {
                        new AlertDialog.Builder(Test.this)
                                .setTitle("Alerte")
                                .setMessage("Un examen a déjà été réalisé à cette date et heure, continuer effacera les données précédentes")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        mode = 1;
                                    }
                                })
                                .show();
                    } else {
                        mode = 0;
                    }
                } catch (Exception e) {
                    System.out.println("erreur : " + e);
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * La fonction btAddPatientOnClick correspond à l'action suivant le clic sur le bouton "AddPattient". <br/>
     * Cette fonction permet de basculer vers la page d'enregistrement d'un nouveau patient si ce dernier n'est pas présent dans la liste.
     *
     * @param view
     */
    public void btAddPatientOnClick(View view) {
        Intent intentNewPatient = new Intent(this, newpatient.class);
        this.startActivity(intentNewPatient);
    }

    public void btStartOnClick(View view) {
        btGo.setEnabled(false);
        Toast.makeText(getApplicationContext(), "Analysing,", Toast.LENGTH_SHORT).show();
        if (!path.exists()) {
            path.mkdirs();
        } else {
            if (new File(path.getPath() + "/Puree.wav").exists()) {
                new File(path.getPath() + "/Puree.wav").delete();
                new File(path.getPath() + "/Eau.wav").delete();
                new File(path.getPath() + "/Yaourt.wav").delete();
            }

            long[] dureesMS = new long[3];
            List<List<Deglutition>> listesDeglutitions = new ArrayList<>();

            Patient patient = new Patient(spPatient.getSelectedItem().toString(), Test.this);
            Examinateur examinateur = new Examinateur(patient.getIdExaminateur(), Test.this);
            String[] quantitees = {(String) spQPuree.getSelectedItem(), (String) spQEau.getSelectedItem(), (String) spQYaourt.getSelectedItem()};

            Spinner[] spTmp = {spPuree, spEau, spYaourt};
            String[] aliments = {"/Puree.wav", "/Eau.wav", "/Yaourt.wav"};
            for (int i = 0; i < 3; i++) {
                try {
                    new File(path + aliments[i]).createNewFile();
                    copy((String) spTmp[i].getSelectedItem(), path + aliments[i]);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            List<Deglutition> listeDeglutitionsPuree = AnalyseRepomp(new Aliment(1, "Puree", 125, 13));
            List<Deglutition> listeDeglutitionsEau = AnalyseRepomp(new Aliment(2, "Eau", 75, 13));
            List<Deglutition> listeDeglutitionsYaourt = AnalyseRepomp(new Aliment(3, "Yaourt", 75, 9));

            if(!listeDeglutitionsPuree.isEmpty()) {
                dureesMS[0] = (long) (listeDeglutitionsPuree.get(listeDeglutitionsPuree.size() - 1).getTimeEndDeglutition() - listeDeglutitionsPuree.get(0).getTimeBeginDeglutition());
            }
            else {
                dureesMS[0] = 0;
            }

            if(!listeDeglutitionsEau.isEmpty()) {
                dureesMS[1] = (long) (listeDeglutitionsEau.get(listeDeglutitionsEau.size() - 1).getTimeEndDeglutition() - listeDeglutitionsEau.get(0).getTimeBeginDeglutition());
            }
            else {
                dureesMS[1] = 0;
            }

            if(!listeDeglutitionsYaourt.isEmpty()) {
                dureesMS[2] = (long) (listeDeglutitionsYaourt.get(listeDeglutitionsYaourt.size() - 1).getTimeEndDeglutition() - listeDeglutitionsYaourt.get(0).getTimeBeginDeglutition());
            }
            else {
                dureesMS[2] = 0;
            }

            listesDeglutitions.add(listeDeglutitionsPuree);
            listesDeglutitions.add(listeDeglutitionsEau);
            listesDeglutitions.add(listeDeglutitionsYaourt);

            HashMap<String, Object> statistiques = Analyse.getStatistics(dureesMS, listesDeglutitions);
            Examen examen = new Examen(patient.getIdPatient(), dateAndHour, quantitees, (String[]) statistiques.get("Durees"), (int[]) statistiques.get("NbDeglus"), (String[]) statistiques.get("DureeMoyenneDeglu"), (String[]) statistiques.get("DureeMoyenneInterDeglu"), this);

            HashMap<String, String> dataForHomePage = new HashMap<>();
            dataForHomePage.put("Patient", patient.getPrenom() + " " + patient.getNom());
            dataForHomePage.put("Date", dateAndHour);
            dataForHomePage.put("Examinateur", examinateur.getNom());
            dataForHomePage.put("Duree", examen.getDurees()[0]);

            int idExamen;
            if (mode == 0) {
                idExamen = examen.save(this);
                examen.setIdExamen(idExamen);

                dataForHomePage.put("idExamen", idExamen + "");
                home.examens.add(0, dataForHomePage);
                home.adapter.notifyDataSetChanged();
            } else {
                idExamen = examen.update(this);
                examen.setIdExamen(idExamen);

                DaoDeglutition bdd = new DaoDeglutition(this);

                bdd.deleteDeglutition(idExamen);
            }

            final HashMap<String, Object> dataForRapport = new HashMap<String, Object>();
            dataForRapport.put("Patient", patient);
            dataForRapport.put("Examen", examen);
            dataForRapport.put("Examinateur", examinateur.getNom());

            if (listeDeglutitionsPuree != null) {
                for (Deglutition deglu : listeDeglutitionsPuree) {
                    deglu.save(idExamen, 1, this);
                }
            }
            if (listeDeglutitionsEau != null) {
                for (Deglutition deglu : listeDeglutitionsEau) {
                    deglu.save(idExamen, 2, this);
                }
            }
            if (listeDeglutitionsYaourt != null) {
                for (Deglutition deglu : listeDeglutitionsYaourt) {
                    deglu.save(idExamen, 3, this);
                }
            }

            new AlertDialog.Builder(this)
                    .setTitle("Analyse")
                    .setMessage("Examen terminé, veuillez patienter le temps que l'examen soit analysé, vous basculerez sur la page du rapport global lorsque celle-ci sera terminée.")
                    .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            File file = new File(path.getPath() + "/Test.wav");
                            file.delete();

                            Intent intentRapport = new Intent(Test.super.getBaseContext(), rapportGlobal.class);
                            intentRapport.putExtra("Examen", dataForRapport);
                            Test.this.finish();
                            startActivity(intentRapport);
                        }
                    })
                    .setCancelable(false)
                    .show();

        }
    }

    private List<Deglutition> AnalyseRepomp(Aliment aliment)//copy paste de Analyse, modification des fichiers d'entrée, et filtre
    {
        int x;
        int count = 0;
        int n = 100;
        int dividende = 0;
        Vector<Double> data = new Vector<>();


        if(aliment == null){
            throw new IllegalArgumentException("Aliment is null");
        }
        n = aliment.getParam1();
        dividende = aliment.getParam2();
        switch (aliment.getLibelle()){
            case "Puree":
                data = Wav.getAmplitude(this, (String) spPuree.getSelectedItem(), checkbox.isChecked());

                break;
            case "Eau":
                data = Wav.getAmplitude(this, (String) spEau.getSelectedItem(), checkbox.isChecked());

                break;
            case "Yaourt":
                data = Wav.getAmplitude(this, (String) spYaourt.getSelectedItem(), checkbox.isChecked());

                break;
        }
        int i = 0;
        double min = 0;
        double max = 0;

        while(i<data.size())
        {
            if(data.get(i) < min)
            {
                min = data.get(i);
            }

            if(data.get(i) > max)
            {
                max = data.get(i);
            }

            i++;
        }

        double condition = max * (double)sbPercent.getProgress() / 100.0;//condition de filtre
        min = min / dividende;
        max = max / dividende;

        try
        {
            List<Deglutition> deglutitions = new ArrayList<Deglutition>();
            int[] deglu = new int[2];
            deglu[0] = -1;

            boolean filtre = false;
            for(x = 0; x < data.size(); x++)
            {
                if(data.get(x) >= condition)
                {
                    filtre = true;
                }
                if((data.get(x) <= min || data.get(x) >= max) && deglu[0] == -1) //Au delà des 2 seuils et début deglutition KO
                {
                    deglu[0] = x; //Début déglutition à l'index x
                    filtre = false;
                    if(data.get(x) >= condition)
                        {
                            filtre = true;
                        }
                }
                else if ((data.get(x) <= min || data.get(x) >= max) && deglu[0] != -1) //Au delà des 2 seuil et début déglutitition OK
                {
                    deglu[1] = x; //Fin de deglutition temporaire
                    count = 0;
                }
                else if((data.get(x) > min || data.get(x) < max) && deglu[0] != -1)  //Entre les 2 seuils et début de déglutition OK
                {
                    if(count == 275) //temps minimal pendant lequel l'amplitude reste entre les 2 seuils pour qu'une déglutition soit considéré comme terminé
                    {
                        if(deglu[1] - deglu[0] > n && filtre) //Vérifie que la durée de la déglutition trouvé est bien supérieure à la durée minimal d'une déglutition (n variant selon aliment, voir début de fonction)
                        {
                            Deglutition deglutition = new Deglutition(deglu[0], deglu[1]);
                            deglutitions.add(deglutition);
                        }

                        deglu[0] = -1;
                        deglu[1] = -1;
                        count = 0;
                    }
                    else
                    {
                        count++;
                    }
                }
            }

            return deglutitions;
        }
        catch (Exception e)
        {
            System.err.println(e);
            return null;
        }
    }


    public static void copy(String srcPath, String destPath) throws IOException {
        InputStream in = new FileInputStream(srcPath);
        try {
            OutputStream out = new FileOutputStream(destPath);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}